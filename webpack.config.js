const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const fs = require('fs-extra');

fs.emptyDirSync('./dist/');

/**
 * @type {webpack.Configuration}
 */
const baseConfig = {
    cache: true,
    mode: 'development', // "production" | "development" | "none"
    module: {
        rules: [{
            // 拡張子 .ts の場合
            test: /\.tsx?$/,
            // TypeScript をコンパイルする
            use: 'ts-loader'
        }, {
            test: /\.tsx?$/,
            enforce: 'pre',
            loader: 'tslint-loader',
            options: {
                configFile: './tslint.json',
                typeCheck: true,
            },
        }, {
            test: /\.css$/,
            use: ['style-loader', 'css-loader'],
        }, ],
    },
    resolve: {
        modules: [
            'node_modules', // node_modules 内も対象とする
        ],
        extensions: [
            '.ts',
            '.tsx',
            '.js', // node_modulesのライブラリ読み込みに必要
        ]
    },
};

const mainConfig = {
    ...baseConfig,
    entry: './src/ts/main.ts',
    target: 'electron-main',
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'main.js'
    },
    plugins: [
        new webpack.DefinePlugin({
            __dirname: '__dirname', // __dirname が "/"が返ってしまう。
        }),
    ],
};

const renderConfig = {
    ...baseConfig,
    entry: './src/ts/index.tsx',
    target: 'electron-renderer',
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'index.js'
    },
    plugins: [
        new webpack.DefinePlugin({
            __dirname: '__dirname', // __dirname が "/"が返ってしまう。
        }),
        new CopyWebpackPlugin(
            [
                './src/html/index.html',
                './src/resources/iconfont/Flaticon.ttf',
                './src/init.js',
                './src/package.json',
                './src/resources/apricot.ico',
            ], {
                to: './dist',
            }
        ),
    ],
};

module.exports = [
    renderConfig,
    mainConfig,
];