# Electron アプリ開発演翁E

## プロジェクト準備

* npmの初期化 package.json の作成
```
PS \> npm init;
```

* Electron と react / redux のインストール
```
PS \> npm install --save-dev electron;
PS \> npm install --save react redux react-redux react-dom;
PS \> npm install --save-dev @types/react @types/redux @types/react-redux @types/react-dom;
```

* gulp のインストール
```
PS \> npm install --global gulp-cli eslint;
PS \> npm install --save-dev gulp @types/gulp;
PS \> New-Item gulpfile.js;
```

* eslint のインストール
```
PS \> .\node_modules\.bin\eslint --init;

? How would you like to configure ESLint? Answer questions about your style
? Are you using ECMAScript 6 features? Yes
? Are you using ES6 modules? Yes
? Where will your code run? Node
? Do you use JSX? No
? What style of indentation do you use? Spaces
? What quotes do you use for strings? Single
? What line endings do you use? Windows
? Do you require semicolons? Yes
? What format do you want your config file to be in? JavaScript
Successfully created .eslintrc.js file in C:\Users\k.ebihara\OneDrive - Ricoh\apricot
```

* typescript と tslint のインストールと初期化
```
PS \> npm install --save-dev typescript tslint gulp-typescript gulp-tslint;
PS \> npm install --save-dev @types/electron @types/gulp-tslint;
PS \> .\node_modules\.bin\tsc.cmd --init;
PS \> tslint --init
```

* sass と sasslint インストール
```
PS \> npm install --save-dev sass
PS \> npm install -g sasslint
PS \> npm install --save normalize.css
```

* その他のライブラリ
```
PS \> npm install --save clone fs-extra
PS \> npm install --save-dev @types/clone @types/fs-extra
```

* ソースディレクトリの作成
```
PS \> mkdir src; mkdir src\ts; mkdir src\sass; mkdir src\html;
```

## HTMLファイルの作�E

```
html:5[tab]
```
