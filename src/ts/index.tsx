import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Provider } from 'react-redux';

import AutoUpdate from './AutoUpdater';
import RootContainer from './components/RootContainer';
import Store from './store/Store';

const reactContainer = document.getElementById('reactContainer');

// 初期
ReactDom.render(
    <Provider store={Store}>
        <RootContainer />
    </Provider>,
    reactContainer,
);

AutoUpdate();
