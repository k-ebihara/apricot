// アプリケーション作�E用のモジュールを読み込み
import { spawn } from 'child_process';
import { app, BrowserWindow, ipcMain, Menu, MenuItemConstructorOptions } from 'electron';
import * as path from 'path';
import * as url from 'url';

// メインウィンドウ
let mainWindow: BrowserWindow | null;

function createWindow() {
    // メインウィンドウ作成する
    mainWindow = new BrowserWindow({
        height: 700,
        width: 1200,
    });

    // メインウィンドウに表示するURLを指定
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true,
    }));

    if (process.argv.find((arg) => arg === '--debug')) {
        // 開発者ツールを同時に開く
        mainWindow.webContents.openDevTools();
    }

    // メインウィンドウが閉じられたときの処理
    mainWindow.on('closed', () => {
        mainWindow = null;
    });
}

//  Electronの初期化が完了したとき
app.on('ready', createWindow);

// 全てのウィンドウが閉じたときの処理
app.on('window-all-closed', () => {
    // macOSのとき以外はこの処理が必要
    if (process.platform !== 'darwin') {
        app.quit();
    }
});
// アプリケーションがアクティブになったとき
app.on('activate', () => {
    // MacOSでは、ドックアイコンがクリックされ、他のウィンドウが開いていないときに、
    // アプリケーションでウィンドウを再作成する必要がある。
    if (mainWindow === null) {
        createWindow();
    }
});

// メニューの作成
const menu: MenuItemConstructorOptions[] = [
    {
        id: 'file',
        label: 'ファイル',
        submenu: [
            {
                accelerator: 'Ctrl+R',
                click: () => {
                    if (!!mainWindow) { mainWindow.reload(); }
                },
                label: 'リロード',
            },
            {
                accelerator: 'Shift+Ctrl+R',
                click: () => {
                    app.relaunch();
                    app.exit(0);
                },
                label: '再起動',
            },
            {
                accelerator: 'F12',
                click: () => {
                    if (mainWindow) {
                        mainWindow.webContents.toggleDevTools();
                    }
                },
                label: '開発者ツールの切り替え',
            },
            {
                click: () => {
                    update();
                },
                enabled: false,
                id: 'update',
                label: 'アップデート',
            },
        ],
    },
];

Menu.setApplicationMenu(
    Menu.buildFromTemplate(menu),
);

let updateFilePath = '';

const update = () => {
    if (!updateFilePath) { return; }
    spawn(updateFilePath, [], {
        detached: true,
    });
    app.quit();
};

ipcMain.on('setUpdatePath', (event: any, args: any[]) => {
    updateFilePath = args[0] as string;
    if (args[1] as boolean) {
        update();
    }
});
