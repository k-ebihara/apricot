import * as React from 'react';
import ReactDOM from 'react-dom';

// tslint:disable-next-line:no-var-requires
const CKEDITOR = require('@ckeditor/ckeditor5-build-classic');

interface IProps {
    html: string;
}

interface IState {
    html: string;
}

export class RichtextEditor extends React.Component<IProps, IState> {
    private editor: any;
    public componentWillMount() {
        this.setState({
            html: this.props.html,
        });
    }
    public componentWillReceiveProps(nextProp: IProps) {
        this.setState({
            html: nextProp.html,
        });
    }
    public render() {
        return (
            <textarea className="editor" ref="editor" value={this.state.html} onChange={() => {/**/ }} />
        );
    }

    public componentDidMount() {
        const e = ReactDOM.findDOMNode(this.refs.editor) as Element;
        CKEDITOR.create(e, { toolbar: [] })
            .then((editor: any) => {
                this.editor = editor;
                this.editor.isReadOnly = true;
                // リンクを外部ブラウザで開くようにする
                const element = this.editor.editing.view.domRoots.get('main') as Element;
                element.querySelectorAll('a[href^=http').forEach((item) => {
                    const anker = item as HTMLAnchorElement;
                    const { href } = anker;
                    anker.onclick = (event) => {
                        Electron.remote.shell.openExternal(href);
                        event.preventDefault();
                    };
                });
            });
    }
    public componentDidUpdate() {
        this.editor.setData(this.state.html);
    }
    public componentWillUnmount() {
        this.editor.destroy();
    }
}
