import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { createTryAction } from '../actions/LoginActions';
import { $colorPrimary3, $colorSecondaryA3, InputText, Label, NextButton } from '../lib/componentParts/CommonStyles';
import { Loading } from '../lib/componentParts/Loading';
import { ElementIdManager } from '../lib/ElementIdManager';
import { ILogin } from '../store/ILogin';
import IState from '../store/IState';
import Store from '../store/Store';

const SITEURL = 'siteurl';
const APPID = 'appid';
const USERID = 'userid';
const PASSWORD = 'password';

//#region Styled

const LoginContent = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    width: 100%;
`;

const LoginForm = styled.div`
    width: 20em;
    margin: 0 auto;
    > p {
        margin: 1em;
    }
`;

const Logo = styled.div`
    font-size: 180%;
    font-weight: bold;
    text-align: center;
`;

const LButton = NextButton.extend`
    width: 100%;
    font-size: 120%;
`;

const LInput = InputText.extend`
    width: 100%;
    font-size: 120%;
`;

//#endregion

interface IStyledProps {
    failLogin?: boolean;
}

const Message = styled.p<IStyledProps>`
    font-weight: bold;
    ${(props: IStyledProps) => { return props.failLogin ? `
            animation-name: identifier;
            animation-delay: 0s;
            animation-duration: 1s;
            animation-fill-mode: forwards;
            color: ${$colorSecondaryA3}` :
        `color: ${$colorPrimary3}`;
    }
    }
`;

// tslint:disable-next-line:no-empty-interface
interface IProps extends ILogin {
}

interface ILocalState {
    appId: string;
    password: string;
    siteUrl: string;
    userId: string;
}

class LoginView extends React.Component<IProps, ILocalState> {
    private idManager = new ElementIdManager();

    public componentWillMount() {
        this.setState({
            appId: this.props.appId,
            password: this.props.password,
            siteUrl: this.props.siteUrl,
            userId: this.props.userId,
        });
    }
    public componentDidMount() {
        this.idManager.createId(SITEURL, APPID, USERID, PASSWORD);
    }
    public render() {
        const idm = this.idManager;
        return (
            <LoginContent>
                {this.props.showLoading ? <Loading /> : null}
                <Logo>
                    <p>EIM デベロッパーサイト<br />
                        質問要望受付アプリ</p>
                </Logo>
                <LoginForm>
                    <p>
                        <Label htmlFor={idm.getId(SITEURL)}>site url</Label>
                        <LInput
                            type="text" name="siteUrl" id={idm.getId(SITEURL)}
                            value={this.state.siteUrl}
                            onChange={this.onChangeTextbox.bind(this, SITEURL)} />
                    </p>
                    <p>
                        <Label htmlFor={idm.getId(APPID)}>appid</Label>
                        <LInput
                            type="text" name="appid" id={idm.getId(APPID)}
                            value={this.state.appId}
                            onChange={this.onChangeTextbox.bind(this, APPID)} />
                    </p>
                    <p>
                        <Label htmlFor={idm.getId(USERID)}>userid</Label>
                        <LInput
                            type="text" name="userid" id={idm.getId(USERID)}
                            value={this.state.userId}
                            onChange={this.onChangeTextbox.bind(this, USERID)} />
                    </p>
                    <p>
                        <Label htmlFor={idm.getId(PASSWORD)}>password</Label>
                        <LInput
                            type="password" name="password" id={idm.getId(PASSWORD)}
                            value={this.state.password}
                            onChange={this.onChangeTextbox.bind(this, PASSWORD)} />
                    </p>
                    <Message failLogin={this.props.failLogin}>
                        <span>
                            {this.props.message}
                        </span>
                    </Message>
                    <p>
                        <LButton
                            type="button" onClick={this.onClickLoginButton.bind(this)}>Login</LButton>
                    </p>
                </LoginForm>
            </LoginContent>
        );
    }
    private onChangeTextbox = (key: string, e: React.ChangeEvent<HTMLInputElement>) => {

        switch (key) {
            case SITEURL:
                this.setState({ siteUrl: e.target.value });
                break;
            case APPID:
                this.setState({ appId: e.target.value });
                break;
            case USERID:
                this.setState({ userId: e.target.value });
                break;
            case PASSWORD:
                this.setState({ password: e.target.value });
                break;
            default:
                return;
        }
    }
    private onClickLoginButton = () => {
        Store.dispatch(createTryAction(
            {
                appId: this.state.appId,
                password: this.state.password,
                siteUrl: this.state.siteUrl,
                userId: this.state.userId,
            },
        ));
    }
}

const mapStateToProps = (state: IState): IProps => {
    return {
        ...state.login,
    };
};

export default connect(mapStateToProps)(LoginView);
