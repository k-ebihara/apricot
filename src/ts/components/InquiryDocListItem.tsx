import * as React from 'react';
import styled from 'styled-components';
import { createDocumentLoadDataAction } from '../actions/DocViewActions';
import { $colorPrimary1, $contentBorder, $defaultFontSize } from '../lib/componentParts/CommonStyles';
import { DateToString } from '../lib/Utils';
import IInquiryDocListItem from '../store/IInquiryDocListItem';
import Store from '../store/Store';
import { IconBug, IconChat, IconPointingUpward, IconQuestion } from './Style';

interface IProps {
    parentCN: string;
    item: IInquiryDocListItem;
}

const ListRow = styled.div`
        border-bottom: ${$contentBorder};
        padding: 0.5em;
        animation: show;
        animation-duration: 0.5s;
        &:hover {
            transition-duration: 0.5s;
            background-color: ${$colorPrimary1};
            cursor: pointer;
        }
        @keyframes show {
            0% {
                opacity: 0;
            }
            100% {
                opacity: 1;
            }
        }
`;

const RowItem = styled.div`
    font-size: 90%;
`;

const Header = RowItem.extend`
    font-size: ${$defaultFontSize};
    font-weight: bold;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
`;
const Asked = RowItem.extend`
    display: flex;
    flex-direction: row;
`;

const Contributor = RowItem.extend`
    flex-grow: 1;
`;

const Posted = RowItem.extend`
    flex-grow: 0;
`;

export class InquiryDocListItem
    extends React.Component<IProps, {}> {
    public render() {
        const { item } = this.props;
        let icon: JSX.Element | null = null;
        switch (item.category) {
            case '質問':
                icon = <IconQuestion />;
                break;
            case '要望':
                icon = <IconChat />;
                break;
            case '意見':
                icon = <IconPointingUpward />;
                break;
            case '障害':
                icon = <IconBug />;
                break;
        }
        return (
            <ListRow key={item.documentId}
                onClick={this.onRowClick.bind(this, item.documentId)}>
                <Header>
                    {icon}
                    {item.title}
                </Header>
                <Asked>
                    <Contributor>by: {item.contributor}</Contributor>
                    <Posted>{DateToString(item.posted)}</Posted>
                </Asked>
                <RowItem>res: {item.personInCharge}</RowItem>
            </ListRow>
        );
    }

    private onRowClick = (documentId: string, e: HTMLElement) => {
        Store.dispatch(createDocumentLoadDataAction(documentId));
    }
}
