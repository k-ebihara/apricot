import styled, { injectGlobal } from 'styled-components';

import * as CommonStyle from '../lib/componentParts/CommonStyles';

import 'normalize.css/normalize.css';
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css';

export const Icon = styled.span`
&:before {
    font-family: Flaticon;
    font-size: 20px;
    font-style: normal;
}
`;

export const IconChat = Icon.extend`
&:before {
    content: "\f100";
    color: ${CommonStyle.$colorSecondaryB1};
 }
`;

export const IconPointingUpward = Icon.extend`
&:before {
    content: "\f101";
    color: ${CommonStyle.$colorPrimary3};
}
`;

export const IconQuestion = Icon.extend`
&:before {
    content: "\f102";
    color: ${CommonStyle.$colorSecondaryA2};
}
`;

export const IconBug = Icon.extend`
&:before {
    content: "\f103";
    color: ${CommonStyle.$colorSecondaryA3};
}
`;

// tslint:disable-next-line:no-unused-expression
injectGlobal`
    @font-face {
        font-family: "Flaticon";
        src: url("./Flaticon.eot");
        src: url("./Flaticon.eot?#iefix") format("embedded-opentype"),
            url("./Flaticon.woff") format("woff"),
            url("./Flaticon.ttf") format("truetype"),
            url("./Flaticon.svg#Flaticon") format("svg");
        font-weight: normal;
        font-style: normal;
    }

    @media screen and (-webkit-min-device-pixel-ratio:0) {
        @font-face {
            font-family: "Flaticon";
            src: url("./Flaticon.svg#Flaticon") format("svg");
        }
    }

    .Toastify__toast--warning {
        color: #333;
    }
`;
