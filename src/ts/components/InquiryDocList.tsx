import Clone from 'clone';
import * as React from 'react';
import * as ReactRedux from 'react-redux';
import styled from 'styled-components';
import { createOpenSerchDialogAction } from '../actions/DialogActions';
import { createAddLoadAction, createLoadDataAction } from '../actions/InquiryDocListActions';
import { $colorForgrandInvert, $colorPrimary1, $colorPrimary3, LinkButton } from '../lib/componentParts/CommonStyles';
import { Loading } from '../lib/componentParts/Loading';
import IInquieryDocList from '../store/IInquiryDocList';
import IState from '../store/IState';
import Store from '../store/Store';
import { InquiryDocListItem } from './InquiryDocListItem';

interface IProps {
    docList: IInquieryDocList;
}

//#region Styled
const Contents = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
`;

const Header = styled.div`
    background-color: ${$colorPrimary3};
    color: ${$colorForgrandInvert};
    padding: 0.5em;
`;

const HeaderActions = styled.div`
    /* background-color: ${$colorPrimary1}; */
`;

const HeaderAction = LinkButton.extend`
    font-size: 90%;
    margin-left: 0.5em;
    &:first-child {
        margin-left: 0;
    }
`;

const Title = styled.p`
    font-size: 110%;
    font-weight: bold;
    margin-bottom: 0.5em;
`;

const DocListContainer = styled.div`
    height: 100%;
    position: relative;
`;

const DocListBody = styled.div`
    overflow-y: scroll;
    height: 100%;
`;
//#endregion

class DocList extends React.Component<IProps, {}> {
    public componentDidMount() {
        Store.dispatch(createLoadDataAction(
            'inquiryAcceptanceView',
            this.props.docList.searchOptions,
        ));
    }

    public render() {
        const cnRoot = 'docList';
        const { docList } = this.props;
        const docItemElements: JSX.Element[] = [];
        docList.docListItems.forEach((item) => {
            docItemElements.push(
                <InquiryDocListItem key={item.documentId} parentCN={cnRoot} item={item} />,
            );
        });
        const loadingScreen = this.props.docList.shownScreen ?
            <Loading /> :
            null;

        return (
            <DocListContainer onScroll={this.onScroll.bind(this)}>
                {loadingScreen}
                <Contents>
                    <Header>
                        <Title>{docList.title}</Title>
                        <HeaderActions>
                            <HeaderAction onClick={this.onClickSearchCondition}>検索条件の変更</HeaderAction>
                            <HeaderAction onClick={this.onClickReload}>更新</HeaderAction>
                        </HeaderActions>
                    </Header>
                    <DocListBody>
                        {docItemElements}
                    </DocListBody>
                </Contents>
            </DocListContainer>
        );
    }

    private onScroll = (e: UIEvent) => {
        if (this.props.docList.shownScreen) {
            return;
        }
        const target = e.target as HTMLElement;
        if (target.scrollHeight - target.scrollTop - target.clientHeight < 1) {
            const searchOptions = Clone(this.props.docList.searchOptions);
            searchOptions.offset = this.props.docList.docListItems.length;
            Store.dispatch(createAddLoadAction(
                'inquiryAcceptanceView',
                searchOptions,
            ));
        }
    }

    private onClickSearchCondition = (e: React.MouseEvent<HTMLElement>) => {
        Store.dispatch(createOpenSerchDialogAction(this.props.docList.searchOptions));
    }

    private onClickReload = (e: React.MouseEvent<HTMLElement>) => {
        Store.dispatch(createLoadDataAction(
            'inquiryAcceptanceView',
            this.props.docList.searchOptions,
        ));
    }
}

const mapStateToProps = (state: IState): IProps => {
    return {
        docList: state.docList,
    };
};

export default ReactRedux.connect(mapStateToProps)(DocList);
