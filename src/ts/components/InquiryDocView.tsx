import classNames from 'classnames';
import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { createChangeTabAction } from '../actions/InquiryDocViewActions';
import { AttachmentFiles } from '../lib/componentParts/AttachmentFiles';
import { $colorBackgraound, $colorForgrandInvert, $colorPrimary1, $colorPrimary3, $colorSecondaryA3, InputSpan, Label, LabelInline } from '../lib/componentParts/CommonStyles';
import { DocumentView } from '../lib/componentParts/DocumentView';
import { ISystem } from '../lib/DocApi';
import { DateToString } from '../lib/Utils';
import IInquiryDoc, { AnswerClass, IInquiryDocProp, Priority, PriorityCode, Tab } from '../store/InquiryDoc';
import IState from '../store/IState';
import Store from '../store/Store';
import { RichtextEditor } from './RichtextEditor';

interface IProps {
    data: IInquiryDoc;
}

//#region Styled
const Content = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
    overflow: hidden;
`;

const Header = styled.div`
    font-size: 90%;
    margin-top: 0.5em;
`;

const HeaderInnerBlock = styled.div`
    display: flex;
    margin-bottom: 1em;
`;

interface IPriority {
    code: PriorityCode | null;
}
const BasicPriority = InputSpan.extend<IPriority>`
    background-color: ${(prop: IPriority) =>
        prop.code === '99' ? $colorSecondaryA3 : 'inherit'};
    color: ${(prop: IPriority) =>
        prop.code === '99' ? $colorForgrandInvert : 'inherit'};
    color: ${(prop: IPriority) =>
        prop.code === '99' ? 'bold' : 'inherit'};
`;

const BasicInfoCategory = InputSpan.extend`
`;

const BasicInfoStatus = InputSpan.extend`
    /* min-width: 5em; */
    font-weight: bold;
`;

const HeaderItem = styled.div`
    margin-right: 1em;
    white-space: nowrap;
`;

const TabHolder = styled.div`
    border-bottom: 1px solid ${$colorPrimary3};
    display: flex;
    flex-direction: row;
    flex-grow: 0;
    flex-shrink: 0;
    justify-content: flex-start;
    align-content: flex-end;
    height: 20pt;
`;

const Container = styled.div`
    display: flex;
    flex-direction: row;
    flex-grow: 1;
    align-items: stretch;
`;

const DocInfo = styled.div`
    overflow-y: auto;
    width: 20em;
    flex-shrink: 0;
    flex-grow: 0;
`;

const DocInfoHeadline = styled.div`
    font-weight: bold;
    margin: 1em 0 0.5em 0;
`;

const DocInfoLabel = LabelInline.extend`
    min-width: 8em;
    display: inline-block;
`;

const Body = styled.div`
    flex-grow: 1;
    overflow-y: auto;
    padding: 0.5em;
    /* height: 100%; */
`;

interface ITabActive {
    active: boolean;
}

const TabPanel = styled.div<ITabActive>`
    border: 1px solid ${$colorPrimary3};
    border-radius: 5px 5px 0 0;
    cursor: pointer;
    margin-left: 0.5em;
    font-size: 110%;
    padding: 0.25em 0.25em 2px 0.25em;
    position: relative;
    top: 1px;
    width: fit-content;
    ${(prop: ITabActive) => {
        return prop.active ? `
            border-bottom-color: ${$colorBackgraound};
            background-color: ${$colorPrimary1};
            top: 5px;
        ` : '';
    }}
`;

//#endregion

class InquiryDocView extends React.Component<IProps, {}> {
    private contentCreator: { [key in Tab]: (docProps: IInquiryDocProp, systemProps: ISystem) => JSX.Element } = {
        answer: (docProps, systemProps) => {
            return (
                <div>
                    <Label>添付ファイル</Label>
                    <AttachmentFiles
                        attachmentFiles={systemProps.attachmentFiles}
                        showFileIds={docProps.answerFile || []} />
                    <RichtextEditor html={docProps.answer ? docProps.answer.html : ''} />
                </div >);
        },
        history: (docProps, systemProps) => {
            return (
                <div>
                    <RichtextEditor html={docProps.history ? docProps.history.html : ''} />
                </div>
            );
        },
        question: (docProps, systemProps) => {
            const headerBlockItemCN = 'inquiryContents_headerBlockItem';
            return (
                <div>
                    <Header>
                        <HeaderInnerBlock>
                            <span className={classNames(headerBlockItemCN, headerBlockItemCN + '_inf')}>
                                <Label>影響範囲</Label>
                                <InputSpan>{docProps.influenceRange || ''}</InputSpan>
                            </span>
                        </HeaderInnerBlock>
                    </Header>
                    <Label>添付ファイル</Label>
                    <AttachmentFiles
                        attachmentFiles={systemProps.attachmentFiles}
                        showFileIds={docProps.detailFile || []} />
                    <RichtextEditor html={docProps.detail ? docProps.detail.html : ''} />
                </div>
            );
        },
        staff: (docProps, systemProps) => {
            return (
                <div>
                    <Label>添付ファイル</Label>
                    <AttachmentFiles
                        attachmentFiles={systemProps.attachmentFiles}
                        showFileIds={docProps.devCommentFile || []} />
                    <RichtextEditor html={docProps.devComment ? docProps.devComment.html : ''} />
                </div>
            );
        },
    };
    public render() {
        const { system, document } = this.props.data;
        const docProps = !!document ? document.properties : null;
        const { activeTab } = this.props.data;
        let content: JSX.Element | null = null;
        if (!!docProps && !!system) {
            content = this.contentCreator[activeTab](docProps, system);
        } else {
            return (
                <DocumentView system={null} showLoading={this.props.data.showLoading} />
            );
        }
        const priorityStr = !docProps.priority ? ''
            : Priority[docProps.priority];
        return (
            <DocumentView
                system={system}
                showLoading={this.props.data.showLoading}>
                <Content key="contents">
                    <TabHolder>
                        <TabPanel active={activeTab === 'question'}
                            onClick={() => { this.onTabClick('question'); }}>問合せ内容</TabPanel>
                        <TabPanel active={activeTab === 'answer'}
                            onClick={() => { this.onTabClick('answer'); }}>回答</TabPanel>
                        <TabPanel active={activeTab === 'staff'}
                            onClick={() => { this.onTabClick('staff'); }}>事務局事項</TabPanel>
                        <TabPanel active={activeTab === 'history'}
                            onClick={() => { this.onTabClick('history'); }}>履歴</TabPanel>
                    </TabHolder>
                    <Container>
                        <Body>
                            {content}
                        </Body>
                        <DocInfo>
                            <DocInfoHeadline>基本情報</DocInfoHeadline>
                            <div>
                                <HeaderItem>
                                    <DocInfoLabel>優先度</DocInfoLabel>
                                    <BasicPriority code={docProps.priority}>{priorityStr}</BasicPriority>
                                </HeaderItem>
                            </div>
                            <div>
                                <HeaderItem>
                                    <DocInfoLabel>ステータス</DocInfoLabel>
                                    <BasicInfoStatus>
                                        {docProps.statusLabel || ''}</BasicInfoStatus>
                                </HeaderItem>
                            </div>
                            <div>
                                <HeaderItem>
                                    <DocInfoLabel>区分</DocInfoLabel>
                                    <BasicInfoCategory>
                                        {docProps.category || ''}</BasicInfoCategory>
                                </HeaderItem>
                            </div>
                            <div>
                                <HeaderItem>
                                    <DocInfoLabel>作成者</DocInfoLabel>
                                    <InputSpan>
                                        {system.createUser.properties.displayName || ''}</InputSpan>
                                </HeaderItem>
                            </div>
                            <div>
                                <HeaderItem>
                                    <DocInfoLabel>担当者</DocInfoLabel>
                                    <InputSpan>
                                        {docProps.assignedUser || ''}</InputSpan>
                                </HeaderItem>
                            </div>
                            <DocInfoHeadline>質問情報</DocInfoHeadline>
                            <div>
                                <HeaderItem>
                                    <DocInfoLabel>デザイナバージョン</DocInfoLabel>
                                    <InputSpan>{docProps.designerVersion || ''}</InputSpan>
                                </HeaderItem>
                            </div>
                            <div>
                                <HeaderItem>
                                    <DocInfoLabel>EIM PFバージョン</DocInfoLabel>
                                    <InputSpan>{docProps.pfVersion || ''}</InputSpan>
                                </HeaderItem>
                            </div>
                            <div>
                                <HeaderItem>
                                    <DocInfoLabel>プロジェクト名</DocInfoLabel>
                                    <InputSpan>{docProps.projectName || ''}</InputSpan>
                                </HeaderItem>
                            </div>
                            <div>
                                <HeaderItem>
                                    <DocInfoLabel>工程</DocInfoLabel>
                                    <InputSpan>{docProps.projectPhase || ''}</InputSpan>
                                </HeaderItem>
                            </div>
                            <div>
                                <HeaderItem>
                                    <DocInfoLabel>完了予定日</DocInfoLabel>
                                    <InputSpan>
                                        {docProps.endScheduledDate ?
                                            DateToString(docProps.endScheduledDate, 'YYYY-MM-DD') : ''}
                                    </InputSpan>
                                </HeaderItem>
                            </div>
                            <DocInfoHeadline>回答情報</DocInfoHeadline>
                            <div>
                                <HeaderItem>

                                    <DocInfoLabel>回答予定日</DocInfoLabel>
                                    <InputSpan>
                                        {docProps.answerDate ?
                                            DateToString(docProps.answerDate, 'YYYY-MM-DD') :
                                            ''}
                                    </InputSpan>
                                </HeaderItem>
                            </div>
                            <div>
                                <HeaderItem>
                                    <DocInfoLabel>対応予定日</DocInfoLabel>
                                    <InputSpan>
                                        {docProps.releaseDate ?
                                            DateToString(docProps.releaseDate, 'YYYY-MM-DD') :
                                            ''}
                                    </InputSpan>
                                </HeaderItem>
                            </div>
                            <DocInfoHeadline>事務局情報</DocInfoHeadline>
                            <div>
                                <HeaderItem>
                                    <DocInfoLabel>受付ユーザー</DocInfoLabel>
                                    <InputSpan>{docProps.firstAcceptanceUser || ''}</InputSpan>
                                </HeaderItem>
                            </div>
                            <div>
                                <HeaderItem>
                                    <DocInfoLabel>受付日時</DocInfoLabel>
                                    <InputSpan>
                                        {docProps.firstAcceptanceDatetime ?
                                            DateToString(docProps.firstAcceptanceDatetime, 'YYYY-MM-DD') :
                                            ''}</InputSpan>
                                </HeaderItem>
                            </div>
                            <div>
                                <HeaderItem>
                                    <DocInfoLabel>担当受付日時</DocInfoLabel>
                                    <InputSpan>
                                        {docProps.assignedDatetime ?
                                            DateToString(docProps.assignedDatetime, 'YYYY-MM-DD') :
                                            ''}</InputSpan>
                                </HeaderItem>
                            </div>
                            <div>
                                <HeaderItem>
                                    <DocInfoLabel>回答区分</DocInfoLabel>
                                    <InputSpan>
                                        {!docProps.answerClass ? '' : AnswerClass[docProps.answerClass]}</InputSpan>
                                </HeaderItem>
                            </div>
                            <div>
                                <HeaderItem>
                                    <DocInfoLabel>回答希望日</DocInfoLabel>
                                    <InputSpan>
                                        {docProps.preferredDate ?
                                            DateToString(docProps.preferredDate, 'YYYY-MM-DD') :
                                            ''}</InputSpan>
                                </HeaderItem>
                            </div>
                            <div>
                                <HeaderItem>
                                    <DocInfoLabel>次回回答予定</DocInfoLabel>
                                    <InputSpan>
                                        {docProps.scheduledDate ?
                                            DateToString(docProps.scheduledDate, 'YYYY-MM-DD') :
                                            ''}</InputSpan>
                                </HeaderItem>
                            </div>

                        </DocInfo>

                    </Container>
                </Content>
            </DocumentView>
        );
    }
    private onTabClick = (tab: Tab) => {
        Store.dispatch(createChangeTabAction(tab));
    }
}

const mapStateToProps = (state: IState): IProps => {
    return {
        data: state.inquiryDoc,
    };
};

export default connect(mapStateToProps)(InquiryDocView);
