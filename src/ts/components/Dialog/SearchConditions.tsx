import Clone from 'clone';
import Moment from 'moment';
import * as React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { createCloseSerchDialogAction, createLoadFavoriteAction, createRemoveFavoriteAction, createSaveFavotieAction } from '../../actions/DialogActions';
import { createLoadDataAction } from '../../actions/InquiryDocListActions';
import { $colorForgrand, $colorForgrandInvert, $colorPrimary2, $colorPrimary3, BackButton, Button, InputText, Label, LabelInline, NextButton } from '../../lib/componentParts/CommonStyles';
import { Dialog } from '../../lib/componentParts/Dialog';
import { DateToString } from '../../lib/Utils';
import { ISearchCondition } from '../../store/IInquiryDocList';
import { Categories, Category, Status, StatusCode, StatusCodes } from '../../store/InquiryDoc';
import { ISearchConditionDialog } from '../../store/ISearchConditionDialog';
import IState from '../../store/IState';
import Store from '../../store/Store';

const ChoiceLabel = LabelInline.extend`
    color: ${$colorForgrand};
    margin-right: 1em;
    &:after {
        content: '';
    }
`;

const Row = styled.p`
    margin-bottom: .5em;
    padding-left: 1em;
`;

const TitleRow = styled.p`
    border-bottom: 1px solid ${$colorForgrand};
    padding-bottom: .5em;
`;

const DateFormat = 'YYYY-MM-DD';

const Container = styled.div`
    display: flex;
    flex-direction: row;
    width: 52em;
`;

const FavoriteColumn = styled.div`
    background-color: ${$colorPrimary3};
    color: ${$colorForgrandInvert};
    flex-grow: 0;
    flex-shrink: 0;
    padding: .5em;
    width: 10em;
`;

const ConditionsContainer = styled.form`
    flex-grow: 1;
    flex-shrink: 1;
    padding: .5em;
`;

const FavoritItem = styled.div`
    cursor: pointer;
    padding: .5em;
    display: flex;
    flex-direction: row;
    &:hover {
        background-color: ${$colorPrimary2};
    }
`;

const FavoritTItle = styled.p`
    flex-grow: 1;
    flex-shrink: 1;
`;

const RemoveFavoritButton = styled.div`
    flex-grow: 0;
    flex-shrink: 0;
    &:after {
        content: '✖'
    }
`;

const SaveFavoriteButton = Button.extend`
    margin-left: 0.5em;
`;

class SearchConditionsDialog extends React.Component<ISearchConditionDialog, ISearchCondition> {
    public componentWillMount() {
        this.setState({
            ...this.props.conditions,
        });
        this.setState({
            offset: 0,
        });
    }

    public componentWillReceiveProps(nextProps: ISearchConditionDialog) {
        this.setState({
            ...nextProps.conditions,
        });
        this.setState({
            offset: 0,
        });
    }

    public render() {
        // ボタン
        const buttons: JSX.Element[] = [
            <BackButton type="button" key="cancel" onClick={this.onClickCancel}>キャンセル</BackButton>,
            <Button type="button" key="clear" onClick={this.onClickClear}>条件をクリア</Button>,
            <NextButton type="button" key="execute" onClick={this.onClickOk}>検索</NextButton>,
        ];
        // カテゴリ
        const categoryChoices =
            Categories.map((category) => {
                return (
                    <ChoiceLabel key={category}>
                        <input type="checkbox" value={category} name="category"
                            checked={!!this.state.category.find((c) => c === category)}
                            onChange={this.onChangeCategory.bind(this, category)}
                        />
                        {category}
                    </ChoiceLabel>);
            });
        // ステータス
        const StatusChoices =
            StatusCodes.map((code) => {
                return (
                    <ChoiceLabel key={code}>
                        <input type="checkbox" value={code} name="status"
                            checked={!!this.state.statusCode.find((c) => c === code)}
                            onChange={this.onChangeStatus.bind(this, code)} />
                        {Status[code]}
                    </ChoiceLabel>
                );
            });
        // お気に入り
        const favorites: JSX.Element[] = this.props.favorites
            .sort((a, b) => {
                return (b.name < a.name) ? 1 : (a.name === b.name) ? -0 : -1;
            })
            .map((f) => {
            return (
                <FavoritItem key={f.id} onClick={this.onClickFavorit.bind(this, f.id)}>
                    <FavoritTItle>{f.name}</FavoritTItle>
                    <RemoveFavoritButton onClick={this.onClickRemoveFavorite.bind(this, f.id)} />
                </FavoritItem>);
        });
        return (
            <Dialog title="検索条件の設定"
                bottomElements={buttons}
                shown={this.props.shown}>
                <Container>
                    <FavoriteColumn>
                        <p>保存したクエリ</p>
                        {favorites}
                    </FavoriteColumn>
                    <ConditionsContainer>
                        <TitleRow>
                            <LabelInline>クエリ名</LabelInline>
                            <InputText onChange={this.onChangeQueryTitle} type="text" value={this.state.queryTitle} />
                            <SaveFavoriteButton type="button" onClick={this.onClickSaveFavorit}>クエリを保存</SaveFavoriteButton>
                        </TitleRow>
                        <p><Label>種別</Label></p>
                        <Row>
                            {categoryChoices}
                        </Row>
                        <p><Label>ステータス</Label></p>
                        <Row>
                            {StatusChoices}
                        </Row>
                        <p><Label>質問者名（部分一致）</Label></p>
                        <Row>
                            <InputText onChange={this.onChangeCreatUser}
                                type="search" value={this.state.creatUserName || ''} />
                        </Row>
                        <p><Label>担当者名（部分一致）</Label></p>
                        <Row>
                            <InputText onChange={this.onChangeAssignedUser}
                                type="search" value={this.state.assignedUser || ''} />
                        </Row>
                        <p><Label>件名（部分一致）</Label></p>
                        <Row>
                            <InputText onChange={this.onChangeTitle}
                                type="search" value={this.state.inquiryTitle || ''} />
                        </Row>
                        <p><Label>問合せ日付</Label></p>
                        <Row>
                            <InputText onChange={this.onChangeInqDateForm}
                                type="date" value={this.state.inquiryDateFrom ?
                                    DateToString(this.state.inquiryDateFrom, DateFormat) : ''} /> ~
                        <InputText onChange={this.onChangeInqDateTo}
                                type="date" value={this.state.inquiryDateTo ?
                                    DateToString(this.state.inquiryDateTo, DateFormat) : ''} />
                        </Row>
                    </ConditionsContainer>
                </Container>
            </Dialog>
        );
    }

    private onClickFavorit = (favoriteId: string, e: React.MouseEvent<HTMLElement>) => {
        Store.dispatch(createLoadFavoriteAction(favoriteId));
        e.stopPropagation();
    }

    private onClickCancel = (e: React.MouseEvent<HTMLElement>) => {
        Store.dispatch(createCloseSerchDialogAction());
    }

    private onClickClear = (e: React.MouseEvent<HTMLElement>) => {
        this.setState({
            assignedUser: '',
            category: [],
            creatUserName: '',
            inquiryDateFrom: null,
            inquiryDateTo: null,
            inquiryTitle: '',
            statusCode: [],
        });
    }

    private onChangeCategory = (data: Category, e: React.ChangeEvent<HTMLInputElement>) => {
        let category = Clone(this.state.category);
        const checked = e.target.checked;
        if (checked) {
            category.push(data);
        } else {
            category = category.filter((c) => c !== data);
        }
        this.setState({
            category,
        });
    }

    private onChangeStatus = (statusCode: StatusCode, e: React.ChangeEvent<HTMLInputElement>) => {
        let statusCodes = Clone(this.state.statusCode);
        const checked = e.target.checked;
        if (checked) {
            statusCodes.push(statusCode);
        } else {
            statusCodes = statusCodes.filter((c) => c !== statusCode);
        }
        this.setState({
            statusCode: statusCodes,
        });
    }

    private onChangeCreatUser = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            creatUserName: e.target.value,
        });
    }

    private onChangeAssignedUser = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            assignedUser: e.target.value,
        });
    }

    private onChangeInqDateForm = (e: React.ChangeEvent<HTMLInputElement>) => {
        const moment = Moment(e.target.value, DateFormat);
        this.setState({
            inquiryDateFrom: moment.isValid() ? moment.toDate() : null,
        });
    }

    private onChangeInqDateTo = (e: React.ChangeEvent<HTMLInputElement>) => {
        const moment = Moment(e.target.value, DateFormat);
        this.setState({
            inquiryDateTo: moment.isValid() ? moment.toDate() : null,
        });
    }

    private onChangeTitle = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            inquiryTitle: e.target.value,
        });
    }

    private onChangeQueryTitle = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            queryTitle: e.target.value,
        });
    }
    private onClickOk = (e: React.MouseEvent<HTMLElement>) => {
        Store.dispatch(createLoadDataAction(
            'inquiryAcceptanceView',
            this.state,
        ));
        Store.dispatch(createCloseSerchDialogAction());
    }

    private onClickSaveFavorit = () => {
        if (!this.state.queryTitle) { return; }
        Store.dispatch(createSaveFavotieAction(this.state));
    }

    private onClickRemoveFavorite = (favoriteId: string, e: React.MouseEvent<HTMLElement>) => {
        Store.dispatch(createRemoveFavoriteAction(favoriteId));
        e.stopPropagation();
    }
}

const mapStateToProps = (state: IState): ISearchConditionDialog => {
    return {
        ...state.searchDialog,
    };
};

export default connect(mapStateToProps)(SearchConditionsDialog);
