import * as React from 'react';
import { connect, Provider } from 'react-redux';
import styled from 'styled-components';

import ReduxToastr from 'react-redux-toastr';
import * as Style from '../lib/componentParts/CommonStyles';
import IState from '../store/IState';
import { IRootContainer } from '../store/RootContainer';
import Store from '../store/Store';
import SearchConditionsDialog from './Dialog/SearchConditions';
import DocList from './InquiryDocList';
import InquiryDocView from './InquiryDocView';
import Login from './Login';

//#region Styled

const MainContainer = styled.div`
    align-items: stretch;
    display: flex;
    flex-direction: column;
    height: 100%;
`;

const Header = styled.div`
    background-color: ${Style.$colorPrimary4};
    color: ${Style.$colorForgrandInvert};
    flex-grow: 0;
`;

const TItle = styled.h1`
    font-size: 120%;
    margin: 0;
    padding: 0.5em;
`;

const Contents = styled.div`
    align-items: stretch;
    display: flex;
    flex-direction: row;
    flex-grow: 1;
    left: 0;
    position: relative;
    top: 0;
`;

const LeftContainer = styled.div`
    border-right: $content-border;
    flex-grow: 0;
    flex-shrink: 0;
    min-width: 200px;
    overflow-y: hidden;
    width: 20em;
    @include posRelative;
`;

const ContentsContainer = styled.div`
    @include posRelative;
    flex-grow: 1;
    overflow: hidden;
    position: relative;
`;

//#endregion

class RootContainer extends React.Component<IRootContainer, {}> {
    public render() {
        let contents: JSX.Element[] = [];
        switch (this.props.viewName) {
            case 'login':
                contents = [
                    <Provider store={Store} key="login">
                        <Login />
                    </Provider>,
                ];
                break;
            case 'main':
                contents = [
                    <LeftContainer key="doclist">
                        <Provider store={Store}>
                            <DocList />
                        </Provider>
                    </LeftContainer>,
                    <ContentsContainer key="docview">
                        <Provider store={Store}>
                            <InquiryDocView />
                        </Provider>
                    </ContentsContainer>,
                ];
                break;
            default:
        }
        return (
            <MainContainer>
                <Header>
                    <TItle>
                        EIM問い合わせAPP
                    </TItle>
                </Header>
                <Contents>
                    {contents}
                </Contents>
                <Provider store={Store}>
                    <SearchConditionsDialog />
                </Provider>
                <ReduxToastr timeOut={5000}
                    position="bottom-right"
                    progressBar={true} />
            </MainContainer>
        );
    }
}

const mapStoStateToProp = (state: IState): IRootContainer => {
    return state.rootContainer;
};

export default connect(mapStoStateToProp)(RootContainer);
