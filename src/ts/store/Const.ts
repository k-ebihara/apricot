/**
 * 質問文書のタイプ
 */
export type inquiryCategory = '質問' | '障害' | '意見' | '要望' | '';

/**
 * 質問文書ステータス
 */
export type inquiryState = '作成中' | '回答中' | '完了' | '';
