import { IDocListViewResultDocData } from '../lib/DocListApi';
import { inquiryCategory, inquiryState } from './Const';

interface IInquiryDocListItem {
    /** 投稿者 */
    contributor: string;
    /** 文書ID */
    documentId: string;
    /** 最終更新日 */
    lastUpdate: Date;
    /** 担当者 */
    personInCharge: string;
    /** 投稿日 */
    posted: Date;
    /** ステータス */
    status: inquiryState;
    /** タイトル */
    title: string;
    /** 文書のタイプ */
    category: inquiryCategory;
}

export const createDocListItem = (source: IDocListViewResultDocData): IInquiryDocListItem => {
    const ret: IInquiryDocListItem = {
        category: '',
        contributor: '',
        documentId: '',
        lastUpdate: new Date(0),
        personInCharge: '',
        posted: new Date(0),
        status: '',
        title: '',
    };
    const colData: { [key: string]: string } = {};
    source.columnValues.forEach((col) => {
        colData[col.propertyName] = col.value;
    });
    ret.category = colData['properties.category'] as inquiryCategory;
    ret.documentId = source.documentId;
    ret.contributor = colData['system.createUser'] || '';
    ret.lastUpdate =
    !!colData['properties.assignedDatetime']
    ? new Date(colData['properties.assignedDatetime'])
    : ret.lastUpdate;
    ret.personInCharge = colData['properties.assignedUser'] || '';
    ret.posted = !!colData['system.createDatetime']
    ? new Date(colData['system.createDatetime'])
        : ret.posted;
    ret.status = colData['properties.statusLabel'] as inquiryState;
    ret.title = colData['properties.title'];
    return ret;
};

export default IInquiryDocListItem;
