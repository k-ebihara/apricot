import userProfile from './UserProfile';

export interface ILogin {
    appId: string;
    failLogin: boolean;
    message: string;
    password: string;
    showLoading: boolean;
    siteUrl: string;
    userId: string;
}

export const getInitLoginModel = (): ILogin => {
    const {site} = userProfile.data;
    return {
        appId: site.appId,
        failLogin: false,
        message: '',
        password: site.password,
        showLoading: false,
        siteUrl: site.siteUrl,
        userId: site.userId,
    };
};

export default ILogin;
