export interface IRootContainer {
    viewName: 'login' | 'main';
}

export const initRootContaine: IRootContainer = {
    viewName: 'login',
};
