import { IDocListViewResult } from '../lib/DocListApi';
import IInquiryDocListItem, { createDocListItem } from './IInquiryDocListItem';
import { Category, StatusCode } from './InquiryDoc';

interface IInquieryDocList {
    docListItems: IInquiryDocListItem[];
    searchOptions: ISearchCondition;
    shownScreen: boolean;
    title: string;
    addDocListItems(docListResult: IDocListViewResult): void;
    setDocListItems(docListResult: IDocListViewResult): void;
}

const MAX_LOAD_COUNT = 30;

export const initDocList: IInquieryDocList = {
    docListItems: [],
    searchOptions: {
        assignedUser: null,
        category: [],
        creatUserName: null,
        inquiryDateFrom: null,
        inquiryDateTo: null,
        inquiryTitle: null,
        limit: MAX_LOAD_COUNT,
        offset: 0,
        queryTitle: '',
        statusCode: [],
    },
    shownScreen: false,
    setDocListItems(docListResult: IDocListViewResult) {
        this.docListItems = [];
        docListResult.docList.forEach((row) => {
            const docListItem: IInquiryDocListItem = createDocListItem(row);
            this.docListItems.push(docListItem);
        });
    },
    addDocListItems(docListResult: IDocListViewResult) {
        docListResult.docList.forEach((row) => {
            const docListItem: IInquiryDocListItem = createDocListItem(row);
            this.docListItems.push(docListItem);
        });
    },
    title: '質問一覧',
};

export interface ISearchCondition {
    /** クエリ名 */
    queryTitle: string;
    /** 取得上限 */
    limit: number;
    /** 取得開始位置 */
    offset: number;
    /** 質問カテゴリ */
    category: Category[];
    /** ステータス */
    statusCode: StatusCode[];
    /** 質問者名(部分一致) */
    creatUserName: string | null;
    /** 担当者名(部分一致) */
    assignedUser: string | null;
    /** 問合せ日From */
    inquiryDateFrom: Date | null;
    /** 問合せ日To */
    inquiryDateTo: Date | null;
    /** タイトル（部分一致） */
    inquiryTitle: string | null;
}

export default IInquieryDocList;
