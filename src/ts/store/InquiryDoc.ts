import { IGetDocResult, IRichTextProperty } from '../lib/DocApi';

export interface IInquiryDoc extends IGetDocResult<IInquiryDocProp> {
    activeTab: Tab;
    showLoading: boolean;
}

export type Tab = 'question' | 'answer' | 'staff' | 'history';

export interface IInquiryDocProp {
    /** 回答本文 */
    answer: IRichTextProperty | null;
    /** 回答区分 */
    answerClass: AnswerClassType | null;
    /** 回答予定日 */
    answerDate: Date | null;
    /** 回答添付ファイル */
    answerFile: string[] | null;
    /** 担当割当日時 */
    assignedDatetime: Date | null;
    /** 担当者名 */
    assignedUser: string | null;
    /** 担当者ID */
    assignedUserId: string | null;
    /** カテゴリ */
    category: Category;
    /** 完了日時 */
    completionDateTime: Date | null;
    /** 作成者組織 */
    createUserOrganizationLabel: string | null;
    /** デザイナバージョン */
    designerVersion: string | null;
    destination: string | null;
    /** 質問詳細 */
    detail: IRichTextProperty | null;
    /** 質問添付ファイル */
    detailFile: string[] | null;
    /** 事務局コメント */
    devComment: IRichTextProperty | null;
    /** 事務局添付ファイル */
    devCommentFile: string[] | null;
    /** 工程完了予定日 */
    endScheduledDate: Date | null;
    /** 初回受付日時 */
    firstAcceptanceDatetime: Date | null;
    /** 初回受付ユーザー */
    firstAcceptanceUser: string | null;
    /** 初回受付ユーザーID */
    firstAcceptanceUserId: string | null;
    /** 履歴 */
    history: IRichTextProperty;
    /** 影響範囲 */
    influenceRange: string | null;
    /** 問い合わせ日時 */
    inquiryDateTime: Date | null;
    /** 問い合わせ番号 */
    inquiryNo: string | null;
    /** 最終回答日 */
    latestAnswerDateTime: Date | null;
    pfVersion: string | null;
    /** 回答希望日 */
    preferredDate: Date | null;
    /** 前回担当者変更日時 */
    prevChangeDateTime: Date | null;
    /** 優先度 */
    priority: PriorityCode | null;
    /** プロジェクト名 */
    projectName: string | null;
    /** プロジェクト工程 */
    projectPhase: string | null;
    /** 対応予定日 */
    releaseDate: Date | null;
    /** 次回回答予定日 */
    scheduledDate: Date | null;
    /** ステータス変更日時 */
    statusChangeDateTime: Date | null;
    /** ステータスコード */
    statusCode: string;
    /** ステータスラベル */
    statusLabel: string;
    /** タグ */
    tag: string | null;
    /** タイトル */
    title: string;
    /** 作成者 */
    writers: string[];
}

export const initInquiryDoc: IInquiryDoc = {
    activeTab: 'question',
    document: {
        properties: null,
    },
    showLoading: false,
    system: null,
};

export type Category = '質問' | '意見' | '要望' | '障害';

export const Categories: Category[] = ['質問', '意見', '要望', '障害'];

export type StatusCode =
    'creating' | 'inquiring' | 'accepted' |
    'answer' | 'informationProvision' | 'closed';

export const StatusCodes: StatusCode[] =
    ['creating', 'inquiring', 'accepted',
    'answer', 'informationProvision', 'closed'];

export const Status: { [code in StatusCode]: string } = {
    accepted: '回答中',
    answer: '回答確認待ち',
    closed: '完了',
    creating: '作成中',
    informationProvision: '情報提供待ち',
    inquiring: '問合せ中',
};

export type PriorityCode = '99' | '50' | '00';

export const Priority: { [code in PriorityCode]: string } = {
    '00': '低',
    '50': '中',
    '99': '高',
};

export type AnswerClassType = 'spec' | 'doc' | 'bug' | 'other';

export const AnswerClass: { [code in AnswerClassType]: string } = {
    bug: '障害',
    doc: '仕様書修正',
    other: 'その他',
    spec: '仕様',
};

export default IInquiryDoc;
