import { ToastrState } from 'react-redux-toastr';
import IInquieryDocList from './IInquiryDocList';
import { ILogin } from './ILogin';
import { IInquiryDoc } from './InquiryDoc';
import { ISearchConditionDialog } from './ISearchConditionDialog';
import { IRootContainer } from './RootContainer';

interface IState {
    docList: IInquieryDocList;
    inquiryDoc: IInquiryDoc;
    login: ILogin;
    rootContainer: IRootContainer;
    searchDialog: ISearchConditionDialog;
    toastr: ToastrState;
}

export default IState;
