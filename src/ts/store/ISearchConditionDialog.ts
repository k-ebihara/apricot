import { ISearchCondition } from './IInquiryDocList';
import userProfile from './UserProfile';

export interface ISearchConditionDialog {
    shown: boolean;
    conditions: ISearchCondition;
    favorites: IFavoriteQueries[];
}

export const getInitSearchCondition = (): ISearchConditionDialog => {
    const condition: ISearchConditionDialog = {
        conditions: {
            assignedUser: null,
            category: [],
            creatUserName: null,
            inquiryDateFrom: null,
            inquiryDateTo: null,
            inquiryTitle: null,
            limit: 30,
            offset: 0,
            queryTitle: '',
            statusCode: [],
        },
        favorites: userProfile.data.favoritQueries,
        shown: false,
    };
    return condition;
};

export interface IFavoriteQueries {
    condition: ISearchCondition;
    id: string;
    name: string;
}

export default ISearchConditionDialog;
