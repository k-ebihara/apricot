import { remote } from 'electron';
import * as ObjectPath from 'object-path';
import { AbstractUserProfile } from '../lib/AbstractUserProfile';
import { IFavoriteQueries } from './ISearchConditionDialog';

class UserProfile extends AbstractUserProfile<IUserProfileData> {
}

// tslint:disable-next-line:max-classes-per-file
export interface IUserProfileData {
    appVersion: string | undefined;
    favoritQueries: IFavoriteQueries[];
    site: ISite;
}

export interface ISite {
    appId: string;
    password: string;
    siteUrl: string;
    userId: string;
}

const createUserProfile = () => {
    const userProfile = new UserProfile(
    {
        appVersion: undefined,
        favoritQueries: [],
        site: {
            appId: 'inquiry',
            password: '',
            siteUrl: 'https://eim-developers.eim-cloud.com',
            userId: '',
        },
    },
    'eim-developers-inquiry');
    const { data } = userProfile;
    // convert
    switch (data.appVersion) {
        case undefined:
            // ver 1.0.1 データレイアウト変更
            const appId = ObjectPath.get(data, 'appId', '');
            const password = ObjectPath.get(data, 'password', '');
            const siteUrl = ObjectPath.get(data, 'siteUrl', '');
            const userId = ObjectPath.get(data, 'userId', '');
            userProfile.data.site = {
                appId,
                password,
                siteUrl,
                userId,
            };
            ObjectPath.del(data, 'appId');
            ObjectPath.del(data, 'password');
            ObjectPath.del(data, 'siteUrl');
            ObjectPath.del(data, 'userId');
            userProfile.save();
            break;
    }
    // init
    const { favoritQueries } = data;
    data.favoritQueries = favoritQueries || [];
    data.appVersion = remote.app.getVersion();
    return userProfile;
};

const singletonUserProfile = createUserProfile();

export default singletonUserProfile;
