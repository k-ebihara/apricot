import * as Redux from 'redux';

import {
    IDocumentLoadDataAction,
    ILoadAttachmentFileInfo,
    IShowDocumentViewAction,
    LOAD_ATTACHMENT_FILE_INFO,
    LOAD_DOCUMENT_DATA,
    SHOW_DOCUMENT_DATA,
} from '../actions/DocViewActions';
import { CHANGE_TAB, IChangeTabAction } from '../actions/InquiryDocViewActions';
import { ActionProcessSwitcher } from '../lib/ActionProcessSwitcher';
import eimApi from '../lib/EimApi';
import { IInquiryDoc, IInquiryDocProp, initInquiryDoc } from '../store/InquiryDoc';

const aps = new ActionProcessSwitcher<IInquiryDoc>();

aps.addAction<IDocumentLoadDataAction>(
    LOAD_DOCUMENT_DATA,
    (state, action) => {
        state.showLoading = true;
        eimApi.getDocData<IInquiryDocProp>(action.documentId)
            .then((data) => {
                if (!!action.onSuccess) { action.onSuccess(data); }
            })
            .catch((err) => {
                if (!!action.onError) { action.onError(err); }
                state.showLoading = false;
            });
        return state;
    },
);

aps.addAction<IShowDocumentViewAction>(
    SHOW_DOCUMENT_DATA,
    (state, action) => {
        state.showLoading = false;
        state.document = action.documentData.document;
        state.system = action.documentData.system;
        state.activeTab = 'question';
        return state;
    },
);

aps.addAction<IChangeTabAction>(
    CHANGE_TAB,
    (state, action) => {
        state.activeTab = action.tab;
        return state;
    },
);

aps.addAction<ILoadAttachmentFileInfo>(
    LOAD_ATTACHMENT_FILE_INFO,
    (state, action) => {
        eimApi.getAttachmentFileInfo(action.fileId)
            .then((result) => {
                if (action.onSuccess) { action.onSuccess(result); }
            });
        return state;
    },
);

const InquiryDocumentViewReducer: Redux.Reducer<IInquiryDoc>
    = (state: IInquiryDoc | undefined, action: Redux.AnyAction) => {
        return aps.processing(state || initInquiryDoc, action);
    };

export default InquiryDocumentViewReducer;
