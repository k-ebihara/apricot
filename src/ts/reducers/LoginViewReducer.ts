import Redux from 'redux';

import * as Action from '../actions/LoginActions';
import { ActionProcessSwitcher } from '../lib/ActionProcessSwitcher';
import eimApi from '../lib/EimApi';
import { IEimApiException } from '../lib/EimApiException';
import ILogin, { getInitLoginModel } from '../store/ILogin';

const aps = new ActionProcessSwitcher<ILogin>();

aps.addAction<Action.ITryAuthAction>(
    Action.TRY_AUTH,
    (state, action) => {
        state.showLoading = true;
        state.failLogin = false;
        eimApi.siteUrl = action.siteUrl;
        eimApi.appId = action.appId;
        eimApi.login(action.userId, action.password)
            .then((data) => {
                if (!!action.onSuccess) { action.onSuccess(data); }
            })
            .catch((e: IEimApiException) => {
                if (!!action.onError) { action.onError(e); }
            });
        return state;
    },
);
aps.addAction<Action.IShowFailLogin>(
    Action.SHOW_FAIL_LOGIN,
    (state, action) => {
        state.showLoading = false;
        state.failLogin = true;
        state.message = action.message;
        return state;
    },
);
aps.addAction<Action.IShowSuccessLogin>(
    Action.SHOW_SUCCESS_LOGIN,
    (state, action) => {
        state.message = '認証成功';
        state.failLogin = false;
        state.showLoading = false;
        return state;
    },
);

export const LoginViewReducer: Redux.Reducer<ILogin, Redux.AnyAction> =
    (state, action) => {
        return aps.processing(state || getInitLoginModel(), action);
};
