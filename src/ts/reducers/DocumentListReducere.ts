import Moment from 'moment';
import * as Redux from 'redux';

import * as DocListAction from '../actions/InquiryDocListActions';
import { ActionProcessSwitcher } from '../lib/ActionProcessSwitcher';
import { DOC_LIST_ACCEPTANCE, IGetDocListDataOptions, ISearchFormula, SearchCondition } from '../lib/DocListApi';
import eimApi from '../lib/EimApi';
import IInquieryDocList, { initDocList, ISearchCondition } from '../store/IInquiryDocList';
import { Status } from '../store/InquiryDoc';

const aps = new ActionProcessSwitcher<IInquieryDocList>();

// データロードを開始する
aps.addAction<DocListAction.ILoadDataAction>(
    DocListAction.START_LOAD_DATA_ACTION,
    (state, action) => {
        if (action.condition) {
            state.searchOptions = action.condition;
        }
        state.shownScreen = true;
        // EIMからデータを取得する
        eimApi.getDocListData(DOC_LIST_ACCEPTANCE, createSearchOptions(state.searchOptions))
            .then((data) => {
                if (!!action.onSuccess) { action.onSuccess(data); }
            })
            .catch((err) => {
                if (!!action.onError) { action.onError(err); }
            });
        return state;
    });
// データを画面に表示する
aps.addAction(
    DocListAction.SHOW_DATA_ACTION,
    (state, action: DocListAction.IShowDataAction) => {
        state.shownScreen = false;
        state.setDocListItems(action.docList);
        return state;
    });
// 追加でロードされたデータを表示する
aps.addAction(
        DocListAction.ADD_DATA_ACTION,
        (state, action: DocListAction.IShowDataAction) => {
            state.shownScreen = false;
            state.addDocListItems(action.docList);
            return state;
        });

const DocumentListReducere: Redux.Reducer<IInquieryDocList>
    = (state: IInquieryDocList | undefined, action: Redux.AnyAction) => {
        return aps.processing(state || initDocList, action);
    };

const createListContainCondition = <T>(list: T[], fn: (value: T) => ISearchFormula): SearchCondition[] => {
        if (list.length === 0) { return []; }
        const searchConditions: SearchCondition[] = [];
        searchConditions.push('(');
        list.forEach((item, index) => {
            if (index !== 0) {
                searchConditions.push('or');
            }
            searchConditions.push(fn(item));
        });
        searchConditions.push(')');
        return searchConditions;
    };

const createSearchOptions = (condition: ISearchCondition) => {
    let searchConditions: SearchCondition[] = [];
    // カテゴリ
    const categoryCondition = createListContainCondition(condition.category,
        (value) => {
            return {
                operator: 'equal',
                propertyName: 'properties.category',
                type: 'string',
                value,
            };
        });
    searchConditions = searchConditions.concat(categoryCondition);
    // ステータス
    if (condition.statusCode.length !== 0) {
        if (searchConditions.length !== 0) { searchConditions.push('and'); }
        const statusCondition = createListContainCondition(condition.statusCode,
            (value) => {
                return {
                    operator: 'equal',
                    propertyName: 'properties.statusLabel',
                    type: 'string',
                    value: Status[value],
                };
            });
        searchConditions = searchConditions.concat(statusCondition);
    }
    // 質問者名(部分一致)
    if (!!condition.creatUserName) {
        if (searchConditions.length !== 0) { searchConditions.push('and'); }
        searchConditions.push({
            operator: 'contain',
            propertyName: 'system.createUser',
            type: 'string',
            value: condition.creatUserName,
        });
    }
    // 担当者名(部分一致)
    if (!!condition.assignedUser) {
        if (searchConditions.length !== 0) { searchConditions.push('and'); }
        searchConditions.push({
            operator: 'contain',
            propertyName: 'properties.assignedUser',
            type: 'string',
            value: condition.assignedUser,
        });
    }
    // 作成日(範囲)
    if (!!condition.inquiryDateFrom || !!condition.inquiryDateTo) {
        if (searchConditions.length !== 0) { searchConditions.push('and'); }
        let {inquiryDateFrom, inquiryDateTo} = condition;
        inquiryDateFrom = inquiryDateFrom || new Date(0);
        inquiryDateTo = inquiryDateTo || new Date('2100-1-1');
        inquiryDateTo = Moment(inquiryDateTo).add(1, 'day').toDate(); // 時刻が 0時なのでその日が含まれないので 1日足す
        searchConditions.push({
            operator: 'range',
            propertyName: 'system.createDatetime',
            type: 'date',
            value: {
                lowerequal: inquiryDateFrom as Date,
                upperequal: inquiryDateTo as Date,
            },
        });
    }
    if (!!condition.inquiryTitle) {
        if (searchConditions.length !== 0) { searchConditions.push('and'); }
        searchConditions.push({
            operator: 'contain',
            propertyName: 'properties.title',
            type: 'string',
            value: condition.inquiryTitle,
        });
    }
    const searchOptions: IGetDocListDataOptions = {
        limit: condition.limit,
        offset: condition.offset,
        search: searchConditions,
        sort: [
            {
                direction: 'Descending',
                propertyName: 'system.createDatetime',
            },
        ],
    };
    return searchOptions;
};

export default DocumentListReducere;
