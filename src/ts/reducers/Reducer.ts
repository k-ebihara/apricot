import { reducer as toastrReducer, ToastrState } from 'react-redux-toastr';
import { AnyAction, combineReducers, Reducer } from 'redux';
import IState from '../store/IState';
import DocumentListReducere from './DocumentListReducere';
import InquiryDocumentViewReducer from './InquiryDocumentViewReducer';
import { LoginViewReducer } from './LoginViewReducer';
import { RootContainerReducer } from './RootContainerReducer';
import { SearchConditionsDialogReducer } from './SearchConditionDialogReducer';

const reducer = combineReducers<IState>({
    docList: DocumentListReducere,
    inquiryDoc: InquiryDocumentViewReducer,
    login: LoginViewReducer,
    rootContainer: RootContainerReducer,
    searchDialog: SearchConditionsDialogReducer,
    toastr: toastrReducer as Reducer<ToastrState, AnyAction>,
});

export default reducer;
