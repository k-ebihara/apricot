import * as Redux from 'redux';

import * as Action from '../actions/RootContainerActions';
import { ActionProcessSwitcher } from '../lib/ActionProcessSwitcher';
import { initRootContaine, IRootContainer } from '../store/RootContainer';

const aps = new ActionProcessSwitcher<IRootContainer>();

aps.addAction<Action.IShowMainAction>(
    Action.SHOW_MAIN,
    (state, action) => {
        state.viewName = 'main';
        return state;
    },
);

export const RootContainerReducer: Redux.Reducer<IRootContainer, Redux.AnyAction> =
    (state, action) => {
        return aps.processing(state || initRootContaine, action);
    };
