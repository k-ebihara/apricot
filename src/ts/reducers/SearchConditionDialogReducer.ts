import * as Redux from 'redux';
import UUID from 'uuid';
import * as Action from '../actions/DialogActions';
import ActionProcessSwitcher from '../lib/ActionProcessSwitcher';
import { getInitSearchCondition, IFavoriteQueries, ISearchConditionDialog } from '../store/ISearchConditionDialog';
import UserProfile from '../store/UserProfile';

const aps = new ActionProcessSwitcher<ISearchConditionDialog>();

aps.addAction<Action.IOpenSearchDialog>(
    Action.OPEN_SEARCH_DIALOG,
    (state, action) => {
        state.shown = true;
        state.conditions = action.condition;
        return state;
    },
);

aps.addAction<Action.ICloseSearchDialog>(
    Action.CLOSE_SEARCH_DIALOG,
    (state, action) => {
        state.shown = false;
        return state;
    },
);

aps.addAction<Action.ILoadFavorite>(
    Action.LOAD_FAVORITE,
    (state, action) => {
        const favorite = state.favorites.find((f) => f.id === action.favoriteId);
        state.conditions = (!!favorite) ? favorite.condition : state.conditions;
        return state;
    },
);

aps.addAction<Action.ISaveFavorite>(
    Action.SAVE_FAVORITE,
    (state, action) => {
        // 同じ名前があったら上書き
        let favorite: IFavoriteQueries;
        const existFavorite = state.favorites.find((f) => f.name === action.conditions.queryTitle);
        if (existFavorite) {
            favorite = existFavorite;
            favorite.condition = action.conditions;
        } else {
            favorite = {
                condition: action.conditions,
                id: UUID.v4(),
                name: action.conditions.queryTitle,
            };
            state.favorites.push(favorite);
        }
        state.conditions = action.conditions;
        UserProfile.data.favoritQueries = state.favorites;
        UserProfile.save();
        return state;
    },
);

aps.addAction<Action.IRemoveFavorite>(
    Action.REMOVE_FAVORITE,
    (state, action) => {
        state.favorites = state.favorites.filter((f) => f.id !== action.favoriteId);
        UserProfile.data.favoritQueries = state.favorites;
        UserProfile.save();
        return state;
    },
);

export const SearchConditionsDialogReducer: Redux.Reducer<ISearchConditionDialog, Redux.AnyAction> =
    (state, action) => {
        return aps.processing(state || getInitSearchCondition(), action);
    };
