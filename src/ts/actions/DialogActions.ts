import * as Redux from 'redux';
import UUID from 'uuid/v4';

import { ISearchCondition } from '../store/IInquiryDocList';

export const OPEN_SEARCH_DIALOG = UUID();

// tslint:disable-next-line:no-empty-interface
export interface IOpenSearchDialog extends Redux.Action {
    condition: ISearchCondition;
}

export const createOpenSerchDialogAction = (condition: ISearchCondition): IOpenSearchDialog => {
    return {
        condition,
        type: OPEN_SEARCH_DIALOG,
    };
};

export const CLOSE_SEARCH_DIALOG = UUID();

// tslint:disable-next-line:no-empty-interface
export interface ICloseSearchDialog extends Redux.Action {
}

export const createCloseSerchDialogAction = (): ICloseSearchDialog => {
    return {
        type: CLOSE_SEARCH_DIALOG,
    };
};

export const LOAD_FAVORITE = UUID();

export interface ILoadFavorite extends Redux.Action {
    favoriteId: string;
}

export const createLoadFavoriteAction = (favoriteId: string): ILoadFavorite => {
    return {
        favoriteId,
        type: LOAD_FAVORITE,
    };
};

export const SAVE_FAVORITE = UUID();

export interface ISaveFavorite extends Redux.Action {
    conditions: ISearchCondition;
}

export const createSaveFavotieAction = (conditions: ISearchCondition): ISaveFavorite => {
    return {
        conditions,
        type: SAVE_FAVORITE,
    };
};

export const REMOVE_FAVORITE = UUID();

export interface IRemoveFavorite extends Redux.Action {
    favoriteId: string;
}

export const createRemoveFavoriteAction = (favoriteId: string): IRemoveFavorite => {
    return {
        favoriteId,
        type: REMOVE_FAVORITE,
    };
};
