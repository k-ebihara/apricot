import Redux from 'redux';
import { IAsyncAction } from '../lib/IAsyncAction';
import Store from '../store/Store';
import userProfile, { ISite } from '../store/UserProfile';
import { createShowMainAction } from './RootContainerActions';

const PREFIX = 'LOGIN';

export const TRY_AUTH = PREFIX + '_TRY_AUTH';

export interface ITryAuthAction extends IAsyncAction<null, Error> {
    appId: string;
    password: string;
    siteUrl: string;
    userId: string;
}

export const createTryAction = (
    data: ISite): ITryAuthAction => {
    return {
        appId: data.appId,
        onError: (e: Error) => {
            Store.dispatch(createShowFailLogin(e.message));
        },
        onSuccess: () => {
            Store.dispatch(createShowSuccessLogin());
            // ちょっと間をおく
            setTimeout(() => {
                Store.dispatch(createShowMainAction());
            }, 500);
            // ユーザープロファイルを更新する
            userProfile.data.site = data;
            userProfile.save();
        },
        password: data.password,
        siteUrl: data.siteUrl,
        type: TRY_AUTH,
        userId: data.userId,
    };
};

export const SHOW_FAIL_LOGIN = PREFIX + '_FAIL_LOGIN';

export interface IShowFailLogin extends Redux.Action {
    message: string;
}

export const createShowFailLogin = (message: string): IShowFailLogin => {
    return {
        message,
        type: SHOW_FAIL_LOGIN,
    };
};

export const SHOW_SUCCESS_LOGIN = PREFIX + '_SUCCESS_LOGIN';

// tslint:disable-next-line:no-empty-interface
export interface IShowSuccessLogin extends Redux.Action {
}

export const createShowSuccessLogin = (): IShowSuccessLogin => {
    return {
        type: SHOW_SUCCESS_LOGIN,
    };
};
