import * as Redux from 'redux';

const PREFIX = 'ROOT_CONTAINER';

export const SHOW_MAIN = PREFIX + '_SHOW_MAIN';

// tslint:disable-next-line:no-empty-interface
export interface IShowMainAction extends Redux.Action {
}

export const createShowMainAction = (): IShowMainAction => {
    return {
        type: SHOW_MAIN,
    };
};
