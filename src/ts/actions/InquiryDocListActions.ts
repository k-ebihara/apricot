import Redux from 'redux';
import { IDocListViewResult } from '../lib/DocListApi';
import { IAsyncAction } from '../lib/IAsyncAction';
import { ISearchCondition } from '../store/IInquiryDocList';
import Store from '../store/Store';

const PREFIX = 'DOCLIST_';
export const START_LOAD_DATA_ACTION = PREFIX + 'LOAD_DATA';

export interface ILoadDataAction extends IAsyncAction<IDocListViewResult, {}> {
    condition: ISearchCondition | null;
    docListKey: string;
}

export const createLoadDataAction =
    (docListKey: string, inquirySearchCondition: ISearchCondition): ILoadDataAction => {

        return {
            condition: inquirySearchCondition,
            docListKey,
            onSuccess: (data) => {
                Store.dispatch(createShowDataAction(data));
            },
            type: START_LOAD_DATA_ACTION,
        };
    };
// 追加のロード
export const createAddLoadAction =
    (docListKey: string, searchOptions: ISearchCondition): ILoadDataAction => {
        return {
            condition: searchOptions,
            docListKey,
            onSuccess: (data) => {
                Store.dispatch(createAddDataAction(data));
            },
            type: START_LOAD_DATA_ACTION,
        };
    };

export const SHOW_DATA_ACTION = PREFIX + 'SHOW_DATA';

export interface IShowDataAction extends Redux.Action {
    docList: IDocListViewResult;
}

const createShowDataAction = (docList: IDocListViewResult): IShowDataAction => {
    return {
        docList,
        type: SHOW_DATA_ACTION,
    };
};

export const ADD_DATA_ACTION = PREFIX + 'ADD_SHOW_DATA';

export interface IAddDataAction extends Redux.Action {
    docList: IDocListViewResult;
}

const createAddDataAction = (docList: IDocListViewResult): IAddDataAction => {
    return {
        docList,
        type: ADD_DATA_ACTION,
    };
};
