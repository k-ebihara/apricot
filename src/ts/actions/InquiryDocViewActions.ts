import * as Redux from 'redux';

import { Tab } from '../store/InquiryDoc';

const PREFIX = 'INQUIRYDOCVIEW';

export const CHANGE_TAB = PREFIX + '_CHANGE_TAB';

export interface IChangeTabAction extends Redux.Action {
    tab: Tab;
}

export const createChangeTabAction = (tab: Tab) => {
    return {
        tab,
        type: CHANGE_TAB,
    };
};
