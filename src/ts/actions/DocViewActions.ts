import * as Electron from 'electron';
import { IGetAttachmentFileInfoResult, IGetDocResult } from '../lib/DocApi';
import { IAsyncAction } from '../lib/IAsyncAction';
import { IInquiryDocProp } from '../store/InquiryDoc';
import Store from '../store/Store';

const PREFIX = 'DOCVIEW';

export const LOAD_DOCUMENT_DATA = PREFIX + '_DOCUMENT_LOAD_DATA';

export interface IDocumentLoadDataAction extends IAsyncAction<IGetDocResult<IInquiryDocProp>, {}> {
    documentId: string;
}

export const createDocumentLoadDataAction =
    (documentId: string): IDocumentLoadDataAction => {
        return {
            documentId,
            onSuccess: (data) => {
                Store.dispatch(createShowDocumentViewAction(data));
            },
            type: LOAD_DOCUMENT_DATA,
        };
    };

export const SHOW_DOCUMENT_DATA = PREFIX + '_SHOW_DOCUMENT_DATA';

export interface IShowDocumentViewAction extends Redux.Action {
    documentData: IGetDocResult<IInquiryDocProp>;
}

export const createShowDocumentViewAction =
    (documentData: IGetDocResult<IInquiryDocProp>): IShowDocumentViewAction => {
        return {
            documentData,
            type: SHOW_DOCUMENT_DATA,
        };
    };

export const LOAD_ATTACHMENT_FILE_INFO = PREFIX + '_LOAD_ATTACHMENT_FILE_INFO';

export interface ILoadAttachmentFileInfo extends IAsyncAction<IGetAttachmentFileInfoResult, {}> {
    fileId: string;
}

export const createLoadAttachmentFileInfoAction = (fileId: string): ILoadAttachmentFileInfo => {
    return {
        fileId,
        onSuccess: (result) => {
            Electron.remote.shell.openExternal(result.url);
        },
        type: LOAD_ATTACHMENT_FILE_INFO,
    };
};
