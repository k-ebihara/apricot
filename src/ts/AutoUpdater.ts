import CompareVersion from 'compare-versions';
import Electron, { ipcRenderer } from 'electron';
import FS from 'fs-extra';
import OS from 'os';
import Path from 'path';
import { toastr } from 'react-redux-toastr';
import request, { Options } from 'request-promise';
import { URL } from 'url';

const baseUrl = 'https://api.bitbucket.org';
const apiPath = '/2.0/repositories/k-ebihara/apricot/downloads';
const updateSiteUrl = new URL(apiPath, baseUrl);
updateSiteUrl.searchParams.append('pagelen', '100');
const manifestFileName = 'manifest.json';
const checkInterval = 60 * 60 * 1000;

let showError = true;
let canceledUpdata: boolean = false;

const updateCheck = () => {
    const options: Options = {
        json: true,
        url: updateSiteUrl.toString(),
    };
    let files: IDownloadData[] = [];
    let targetFileName = '';
    const tempDir = Path.join(OS.tmpdir(), Electron.remote.app.getName());
    let targetFilePath = '';
    request(options)
        .then((data: IResultDownLoads) => {
            let manifestUrl: string = '';
            files = data.values;
            const manifestData = files.find((fileData) => fileData.name === manifestFileName);
            if (!!manifestData) {
                manifestUrl = manifestData.links.self.href;
            }
            if (!!manifestUrl) {
                const o: Options = {
                    json: true,
                    url: manifestUrl,
                };
                return request(o);
            } else {
                throw new Error('manifest file not found.');
            }
        })
        .then((data: IManifest) => {
            const appVersion = Electron.remote.app.getVersion();

            if (CompareVersion(data.version, appVersion) === 1) {
                targetFileName = data.fileName;
                targetFilePath = Path.join(tempDir, targetFileName);
                if (!FS.pathExistsSync(targetFilePath)) {
                    // 未だダウンロードしていない
                    const installer = files.find((fileData) => fileData.name === data.fileName);
                    if (installer) {
                        toastr.info('Auto update', '新しいバージョンをダウンロードしています。');
                        canceledUpdata = false;
                        const o: Options = {
                            encoding: null,
                            url: installer.links.self.href,
                        };
                        return request(o);
                    } else {
                        throw new Error(`installer file not found.(${data.fileName})`);
                    }
                }
            } else {
                setTimeout(updateCheck, checkInterval);
            }
        })
        .then((data: any) => {
            if (!data || !targetFilePath) { return; }
            FS.emptyDirSync(tempDir);
            // // temp dir を取得する
            return FS.writeFile(targetFilePath, data);
        })
        .then(() => {
            if (!FS.pathExistsSync(targetFilePath) || canceledUpdata) { return; }
            // インストールするか聞く
            toastr.confirm(
                `新しいバージョンをインストールしますか？
                「OK」を押すとアプリを終了し、インストーラーを起動します。`, {
                    onCancel: () => {
                        canceledUpdata = true;
                        const menu = Electron.remote.Menu.getApplicationMenu();
                        if (!menu) { return; }
                        const updateMenu = menu.getMenuItemById('update');
                        updateMenu.enabled = true;
                        update(targetFilePath, false);
                        setTimeout(updateCheck, checkInterval);
                    },
                    onOk: () => {
                        update(targetFilePath, true);
                    },
                });
        })
        .catch((e: Error) => {
            switch (e.message) {
                case 'cancel':
                    break;
                default:
                    if (showError) {
                        toastr.warning('Auto update', e.message);
                    }
            }
            showError = false;
            setTimeout(updateCheck, checkInterval);
        });
};

const update = (filePath: string, immediately: boolean) => {
    ipcRenderer.send('setUpdatePath', [filePath, immediately]);
};

interface IResultGetList<T> {
    size: number;
    page: number;
    pagelen: number;
    next: string;
    previous: string;
    values: T[];
}

interface IDownloadData {
    name: string;
    links: {
        self: {
            href: string;
        };
    };
}

interface IResultDownLoads extends IResultGetList<IDownloadData> {
}

interface IManifest {
    version: string;
    fileName: string;
}

export default updateCheck;
