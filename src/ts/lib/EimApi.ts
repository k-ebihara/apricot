import { Response } from 'request';
import RequestPromise from 'request-promise';
import Format from 'string-template';
import { URL } from 'url';
import { ATTACHMENT_FILE_API, DOC_API_URL, IGetAttachmentFileInfoResult, IGetDocResult } from './DocApi';
import { DOC_LIST_API_URL, IDocListViewResult, IGetDocListDataOptions } from './DocListApi';
import * as EimApiException from './EimApiException';
import { ERR_NOT_AUTH } from './EimApiException';

const EIM_API_URL = {
    /** 文書取得URL */
    GET_DOC: DOC_API_URL,
    /** 文書リストデータ取得 */
    GET_DOC_LIST_DATA: DOC_LIST_API_URL,
    /** ログイン */
    LOGIN: '/services/v1/login',
};

export class EimApi {
    public siteUrl: string = '';
    public appId: string = '';
    public port: number = 80;
    public token: string = '';

    public login = (user: string, password: string) => {
        const url = new URL(EIM_API_URL.LOGIN, this.siteUrl);
        const option: RequestPromise.Options = {
            form: {
                ['userName']: user,
                ['password']: password,
            },
            method: 'POST',
            port: this.port,
            resolveWithFullResponse: true,
            simple: false,
            url,
        };
        return new Promise<null>((resolve, reject) => {
            RequestPromise(option)
                .then((response: Response) => {
                    const statusStr = String(response.statusCode);
                    if (response.statusCode === 303) {
                        // ヘッダに set cookie があればOK
                        const setCookies = response.headers['set-cookie'] || [];
                        const findCookie = setCookies.find((cookie) => cookie.startsWith('APISID='));
                        if (!!findCookie) {
                            this.token = findCookie;
                            resolve();
                        } else {
                            reject(EimApiException.ERR_NOT_AUTH);
                        }
                    } else if (statusStr.startsWith('4')) {
                        // tslint:disable-next-line:no-console
                        console.warn(response.headers);
                        reject(EimApiException.ERR_REQUEST_ERROR);
                    } else if (statusStr.startsWith('5')) {
                        // tslint:disable-next-line:no-console
                        console.warn(response.headers);
                        reject(EimApiException.ERR_SERVER_ERROR);
                    } else {
                        // tslint:disable-next-line:no-console
                        console.warn(response.headers);
                        reject(EimApiException.ERR_UNEXPECTED);
                    }
                })
                .catch((reson) => {
                    // tslint:disable-next-line:no-console
                    console.warn(reson);
                    reject(EimApiException.ERR_NETWORK_ERROR);
                });
        });
    }

    public getDocListData = (docListKey: string, pOptions: IGetDocListDataOptions) => {
        return new Promise<IDocListViewResult>((resolve, reject) => {
            const urlDir = Format(DOC_LIST_API_URL, ({
                appId: this.appId,
                docListKey,
            }));
            const url = new URL(urlDir, this.siteUrl);
            const cookie = RequestPromise.cookie(this.token);
            const jar = RequestPromise.jar();
            if (!!cookie) {
                jar.setCookie(cookie, this.siteUrl);
            }
            const options: RequestPromise.Options = {
                body: pOptions,
                jar,
                json: true,
                method: 'POST',
                port: this.port,
                resolveWithFullResponse: true,
                simple: false,
                url,
            };
            RequestPromise(options)
                .then((data: Response) => {
                    const statusCodeStr = String(data.statusCode);
                    if (statusCodeStr === '200') {
                        resolve(data.body as IDocListViewResult);
                    } else if (statusCodeStr.startsWith('4')) {
                        reject(ERR_NOT_AUTH);
                    } else if (statusCodeStr.startsWith('5')) {
                        reject(EimApiException.ERR_SERVER_ERROR);
                    } else {
                        reject();
                    }
                })
                .catch((e) => {
                    //
                });
        });
    }

    public getDocData = <T>(documentId: string) => {
        return new Promise<IGetDocResult<T>>((resolve, reject) => {
            const urlDir = Format(DOC_API_URL, ({
                appId: this.appId,
                documentId,
            }));
            const url = new URL(urlDir, this.siteUrl);
            const cookie = RequestPromise.cookie(this.token);
            const jar = RequestPromise.jar();
            if (!!cookie) {
                jar.setCookie(cookie, this.siteUrl);
            }
            const options: RequestPromise.Options = {
                jar,
                json: true,
                method: 'GET',
                port: this.port,
                resolveWithFullResponse: true,
                simple: false,
                url,
            };
            RequestPromise(options)
                .then((data: Response) => {
                    const inquiryData = JSON.stringify(data.body);
                    const json = JSON.parse(inquiryData, (key, value) => {
                        // 2018-04-19T01:20:51.171Z
                        if (typeof (value) !== 'string') {
                            return value;
                        }
                        const reg = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/;
                        if (reg.test(value as string)) {
                            return new Date(value);
                        } else {
                            return value;
                        }
                    }) as IGetDocResult<T>;
                    resolve(json);
                });
        });
    }

    public getAttachmentFileInfo = (fileId: string) => {
        return new Promise<IGetAttachmentFileInfoResult>((resolve, reject) => {
            const urlDir = Format(ATTACHMENT_FILE_API, ({
                fileId,
            }));
            const url = new URL(urlDir, this.siteUrl);

            const cookie = RequestPromise.cookie(this.token);
            const jar = RequestPromise.jar();
            if (!!cookie) {
                jar.setCookie(cookie, this.siteUrl);
            }
            const options: RequestPromise.Options = {
                jar,
                json: true,
                method: 'GET',
                port: this.port,
                resolveWithFullResponse: true,
                simple: false,
                url,
            };
            RequestPromise(options)
                .then((data: Response) => {
                    const inquiryData = JSON.stringify(data.body);
                    const json = JSON.parse(inquiryData, (key, value) => {
                        // 2018-04-19T01:20:51.171Z
                        if (typeof (value) !== 'string') {
                            return value;
                        }
                        const reg = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/;
                        if (reg.test(value as string)) {
                            return new Date(value);
                        } else {
                            return value;
                        }
                    }) as IGetAttachmentFileInfoResult;
                    resolve(json);
                });
        });
    }
}

// シングルトンオブジェクト
const eimApi = new EimApi();

export default eimApi;
