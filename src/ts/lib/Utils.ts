import Moment from 'moment';

export const DateToString = (mdate: Date, format: string = ''): string => {
    const now = Moment();
    const today = Moment().endOf('day');
    const date = Moment(mdate);
    if (!!format) {
        return date.format(format);
    }
    // 3分以内 = さっき
    if (now.diff(date, 'minutes') < 3) {
        return 'さっき';
    }
    // 1時間以内 = ?? 分前
    if (now.diff(date, 'hours') < 1) {
        return now.diff(date, 'minutes') + '分前';
    }
    // 今日 = ?? 時 ?分
    if (today.diff(date, 'day') < 1) {
        return date.format('H時 m分');
    }
    // 昨日 = 昨日 ?? 時
    if (today.diff(date, 'day') < 2) {
        return '昨日 ' + date.hour() + '時';
    }
    // 一昨日 ～ 7日前 = ?日 ?時
    if (today.diff(date, 'day') < 7) {
        return date.format('D日 H時');
    }
    // 4日 ～ 今月内 ??日
    if (today.startOf('month').diff(date) < 0) {
        return date.format('D日');
    }
    // 前月～1年前 ??月??日
    if (Moment().diff(date, 'month') < 12) {
        return date.format('M月D日');
    }
    // 一年前 ?年前
    return date.format('YYYY年M月');
};
