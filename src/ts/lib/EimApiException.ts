export interface IEimApiException extends Error {
    id: string;
}

class EimApiException extends Error implements IEimApiException {
    public id: string;
    constructor(id: string, message: string) {
        super(message);
        this.id = id;
    }
}

export const ERR_NOT_AUTH = new EimApiException('not-authentication', '認証に失敗しました。');
export const ERR_NETWORK_ERROR = new EimApiException('network-error', 'サーバーにアクセスできません。');
export const ERR_SERVER_ERROR = new EimApiException('server-error', 'サーバー内でエラーが発生しました。');
export const ERR_REQUEST_ERROR = new EimApiException('request-error', 'リクエスト形式が間違っています。');
export const ERR_UNEXPECTED = new EimApiException('unexpected-error', '予期しないエラーです。');
