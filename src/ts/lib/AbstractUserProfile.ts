import Clone from 'clone';
import Electron from 'electron';
import Fs from 'fs-extra';
import Path from 'path';

// tslint:disable-next-line:no-var-requires
export abstract class AbstractUserProfile<T> {
    public readonly appName: string;
    public data: T;
    private initData: T;
    public constructor(initData: T, appName: string) {
        this.initData = initData;
        this.data = Clone(initData);
        this.appName = appName;
        this.load();
    }
    public save() {
        const profilePath = this.getUserProfilePath();
        Fs.ensureDir(Path.dirname(profilePath))
            .then(() => {
                return Fs.writeJSON(profilePath, this.data);
            })
            .catch((e) => {
                // tslint:disable-next-line:no-console
                console.error(e);
            });
    }
    private load() {
        const profilePath = this.getUserProfilePath();
        const exist = Fs.pathExistsSync(profilePath);
        if (exist) {
            try {
                const loadData = Fs.readJSONSync(profilePath) as T;
                this.data = Object.assign(this.initData, loadData);
            } catch (e) {
                this.data = Clone(this.initData);
                // tslint:disable-next-line:no-console
                console.error(e);
            }
        }
    }
    private getUserProfilePath(): string {
        const process = Electron.remote.process;
        const fileName = 'profile.json';
        if (process) {
            if (process.platform === 'win32') {
                // tslint:disable-next-line:no-string-literal
                return Path.join(process.env['APPDATA'], this.appName, fileName);
            } else {
                // tslint:disable-next-line:no-string-literal
                return Path.join(process.env['HOME'], this.appName, fileName);
            }
        } else {
            return '';
        }
    }
}
