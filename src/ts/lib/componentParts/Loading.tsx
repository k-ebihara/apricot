import { ColorProperty } from 'csstype';
import React from 'react';
import styled, { keyframes } from 'styled-components';

import { $colorPrimary4 } from '../../lib/componentParts/CommonStyles';

//#region Styled

const LoadingContent = styled.div`
    position: absolute;
    display: flex;
    width: 100%;
    height: 100%;
    background-color: rgba(196, 196, 196, 0.5);
    z-index: 1000;
`;

const LoadingRoot = styled.div`
    position: relative;
    width: 58px;
    height: 58px;
    margin: auto;
`;

const BounceCircularG = keyframes`
    0% {
        opacity: 1;
        transform: scale(1);
    }
    100% {
        opacity: 0;
        transform: scale(0);
    }
`;

const CircularG = styled.div`
    background-color: ${$colorPrimary4};
    position: absolute;
    width: 14px;
    height: 14px;
    border-radius: 9px;
    animation-name: ${BounceCircularG};
    animation-duration: 0.8s;
    animation-iteration-count: infinite;
    animation-direction: normal;
    opacity: 0;
`;

const CircularG1 = CircularG.extend`
    left: 0;
    top: 23px;
    animation-delay: 0.0s;
    transform: scale(0);
`;

const CircularG2 = CircularG.extend`
    left: 6px;
    top: 6px;
    animation-delay: 0.10s;
    transform: scale(0);
`;

const CircularG3 = CircularG.extend`
    left: 23px;
    top: 0px;
    animation-delay: 0.20s;
    transform: scale(0);
`;

const CircularG4 = CircularG.extend`
    right: 6px;
    top: 6px;
    animation-delay: 0.30s;
    transform: scale(0);
`;

const CircularG5 = CircularG.extend`
    right: 0px;
    top: 23px;
    animation-delay: 0.40s;
    transform: scale(0);
`;

const CircularG6 = CircularG.extend`
    right: 6px;
    bottom: 6px;
    animation-delay: 0.50s;
    transform: scale(0);
`;

const CircularG7 = CircularG.extend`
    left: 23px;
    bottom: 0px;
    animation-delay: 0.60s;
    transform: scale(0);
`;

const CircularG8 = CircularG.extend`
    left: 6px;
    bottom: 6px;
    animation-delay: 0.70s;
    transform: scale(0);
`;

interface IProp {
    color?: ColorProperty;
}

//#endregion

export class Loading extends React.Component<IProp, {}> {
    public render() {
        return (
            <LoadingContent>
                <LoadingRoot>
                    <CircularG1 />
                    <CircularG2 />
                    <CircularG3 />
                    <CircularG4 />
                    <CircularG5 />
                    <CircularG6 />
                    <CircularG7 />
                    <CircularG8 />
                </LoadingRoot>
            </LoadingContent>);
    }
}
