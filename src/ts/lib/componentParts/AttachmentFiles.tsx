import React, { MouseEvent } from 'react';

import styled from 'styled-components';
import { createLoadAttachmentFileInfoAction } from '../../actions/DocViewActions';
import Store from '../../store/Store';
import { IAttachemntFile } from '../DocApi';
import { $colorPrimary0, $colorPrimary3 } from './CommonStyles';

interface IProps {
    attachmentFiles: IAttachemntFile[];
    showFileIds: string[];
}

const FileContainer = styled.div`
    border: 1px solid ${$colorPrimary0};
    padding: 0.5em;
`;

const FileItem = styled.div`
    color: ${$colorPrimary3};
    cursor: pointer;
    &:hover {
        text-decoration: underline;
    }
`;

export class AttachmentFiles extends React.Component<IProps, {}> {
    public render() {
        const files = this.props.attachmentFiles
            .filter((file) => {
                return this.props.showFileIds.find((fileId) => fileId === file.fileId);
            });
        if (files.length === 0) {
            return <p>なし</p>;
        }
        return (
            <FileContainer>{
                    files.map((file) => {
                    const onclick = (e: MouseEvent<HTMLSpanElement>) => {
                        Store.dispatch(createLoadAttachmentFileInfoAction(file.fileId));
                        e.preventDefault();
                    };
                    return (
                        <FileItem key={file.fileId}
                            onClick={onclick}>
                            {file.fileName}
                        </FileItem>
                    );
                })
            }</FileContainer>
        );
    }
}
