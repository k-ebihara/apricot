import * as React from 'react';
import styled, { keyframes } from 'styled-components';

import { createCloseSerchDialogAction } from '../../actions/DialogActions';
import {
    $colorBackgraound,
    $colorForgrandInvert,
    $colorPrimary1,
    $colorPrimary3,
} from '../../lib/componentParts/CommonStyles';
import Store from '../../store/Store';

//#region Styled
const BackScreen = styled.div`
    align-items: center;
    background-color: rgba(0,0,0,.5);
    display: flex;
    flex-direction: row;
    height: 100%;
    justify-content: center;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
    z-index: 1000;

`;

const showDialogKF = keyframes`
    0% {
        transform: scale(0.3);
    }

    100% {
        transform: scale(1);
    }
`;

const DialogWindow = styled.div`
    animation-duration: 0.3s;
    animation-iteration-count: 1;
    animation-name: ${showDialogKF};
    background-color: ${$colorBackgraound};
    border: 2px solid ${$colorPrimary3};
    border-radius: 5px;
    display: flex;
    flex-direction: column;
    min-height: 200px;
    min-width: 300px;
    opacity: 1;
`;

const TitleBar = styled.div`
    background-color: ${$colorPrimary3};
    color: ${$colorForgrandInvert};
    font-size: 120%;
    padding: 0.25em;
    display: flex;
    flex-direction: row;
`;

const HeaderTitle = styled.div`
    flex-grow: 1;
    flex-shrink: 1;
`;

const CloseButton = styled.div`
    flex-grow: 0;
    flex-shrink: 0;
    cursor: pointer;
`;

const Content = styled.div`
    flex-grow: 1;
    padding: 0.25em;
`;

const BottomActions = styled.div`
    border-top: solid 1px ${$colorPrimary1};
    display: flex;
    justify-content: space-around;
    padding: 0.25em;
`;
//#endregion

interface IProps {
    title: string;
    bottomElements?: JSX.Element[];
    shown: boolean;
}

export class Dialog extends React.Component<IProps, {}> {
    public render() {
        if (!this.props.shown) { return null; }
        return (
            <BackScreen className="backscreen" onClick={this.onClickScreen}>
                <DialogWindow>
                    <TitleBar>
                        <HeaderTitle>{this.props.title}</HeaderTitle>
                        <CloseButton onClick={this.onClickClose}>✖</CloseButton>
                    </TitleBar>
                    <Content>
                        {this.props.children}
                    </Content>
                    <BottomActions>
                        {this.props.bottomElements}
                    </BottomActions>
                </DialogWindow>
            </BackScreen>
        );
    }

    private onClickScreen = (e: React.MouseEvent<HTMLElement>) => {
        if ((e.target as HTMLElement).classList.contains('backscreen')) {
            Store.dispatch(createCloseSerchDialogAction());
        }
    }

    private onClickClose = (e: React.MouseEvent<HTMLElement>) => {
        Store.dispatch(createCloseSerchDialogAction());
    }
}
