import Electron from 'electron';
import * as React from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';
import URL from 'url';

import { ToolTip } from '../../lib/componentParts/CommonStyles';
import { ISystem } from '../DocApi';
import eimApi from '../EimApi';
import { DateToString } from '../Utils';
import { Loading } from './Loading';

interface IProps {
    system: ISystem | null;
    showLoading: boolean;
}

//#region Styled

const DocView = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
`;

const Header = styled.div`
    flex-grow: 0;
    background-color: $color-primary-3;
    padding: 0.5em;
    color: $color-forgrand-invert;
`;

const HeaderTitle = styled.h1`
    font-size: 150%;
    margin: 0;
`;

const HeaderInfo = styled.div`
    display: flex;
    flex-direction: row-reverse;
    >div {
        padding-left: 1em;
    }
`;

const HeaderLink = styled.a`
    color: $color-forgrand-invert !important;
    margin-right: 1em;
    font-size: 11pt;
    font-weight: normal;
`;

const Contents = styled.div`
    flex-grow: 1;
    height: 100%;
    overflow-y: auto;
    padding: 0.5em;
`;
////#endregion

export class DocumentView extends React.Component<IProps, {}> {
    public render() {
        const { system } = this.props;
        const { showLoading } = this.props;
        // スクリーン表示
        if (showLoading === false &&
            (document === null || system === null)) {
            return (
                <div>ドキュメントが選択されていません</div>
            );
        }
        const loading = showLoading ?
            <Loading /> :
            null;
        const content = this.createContent(system);
        return (
            <DocView>
                {loading}
                {content}
            </DocView>
        );
    }
    public componentDidUpdate() {
        const docroot = this.refs.docroot;
        const element = ReactDOM.findDOMNode(docroot) as Element | null;
        if (!element) { return; }
        element.scrollTo(0, 0);
        element.querySelectorAll('a[href^=http').forEach((item) => {
            const anker = item as HTMLAnchorElement;
            const { href } = anker;
            anker.onclick = (event) => {
                Electron.remote.shell.openExternal(href);
                event.preventDefault();
            };
        });
    }
    private createContent(system: ISystem | null) {
        if (!system) { return null; }
        const docUrl = new URL.URL(eimApi.siteUrl);
        docUrl.hash = `/${eimApi.appId}/documents/${system.documentId}`;
        return ([
            <Header ref="docroot" key="header">
                <HeaderTitle>
                    {system.title}
                    <HeaderLink href={docUrl.toString()}>[ブラウザで表示]</HeaderLink>
                </HeaderTitle>
                <HeaderInfo>
                    <ToolTip
                        data-tooltip={DateToString(system.createDatetime, 'YYYY-MM-DD HH:mm')}>
                        作成日: {DateToString(system.createDatetime)}
                    </ToolTip>
                    <div>
                        作成者: {system.createUser.properties.displayName}
                    </div>
                </HeaderInfo>
            </Header>,
            <Contents key="contents">
                {this.props.children}
            </Contents>,
        ]);
    }
}
