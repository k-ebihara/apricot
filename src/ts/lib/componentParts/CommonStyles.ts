import { desaturate, lighten } from 'polished';
import styled, { injectGlobal } from 'styled-components';

// http://paletton.com/#uid=32q0u0kieip9awdeQoqn6eqsf8R

export const $colorPrimary0 = '#6A873A';	// Main Primary color */
export const $colorPrimary1 = '#D2ECA8';
export const $colorPrimary2 = '#94B460';
export const $colorPrimary3 = '#4D6A1D';
export const $colorPrimary4 = '#2B4108';

export const $colorSecondaryA0 = '#93473F';	// Main Secondary color (1) */
export const $colorSecondaryA1 = '#FFBDB6';
export const $colorSecondaryA2 = '#C37168';
export const $colorSecondaryA3 = '#732820';
export const $colorSecondaryA4 = '#460E08';

export const $colorSecondaryB0 = '#522C61';	// Main Secondary color (2) */
export const $colorSecondaryB1 = '#9D7BAA';
export const $colorSecondaryB2 = '#714881';
export const $colorSecondaryB3 = '#3D184C';
export const $colorSecondaryB4 = '#24072F';

export const $colorForgrand = '#333';
export const $colorForgrandInvert = '#ddd';
export const $colorBackgraound = '#fff';

export const $contentBorder = `1px solid ${$colorPrimary2}`;
export const $defaultFontSize = '10pt';

/** 標準ボタン */
export const Button = styled.button`
    background-color: ${$colorPrimary3};
    /*ボタン色*/
    border: 0 solid #000;
    border-bottom: solid 2px ${$colorPrimary4};
    border-radius: 3px;
    color: ${$colorForgrandInvert};
    cursor: pointer;
    display: inline-block;
    padding: 0.5em 1em;
    text-decoration: none;
    transition: all 300ms 0ms ease;
    &:hover {
        color: ${$colorForgrand};
        background-color: ${$colorPrimary2};
        border-bottom-color: ${$colorPrimary3};
    }
    &:active {
        background-color: ${$colorPrimary1};
    }
    &:focus {
        outline: none !important;
    }
`;

export const NextButton = Button.extend`
    &:after {
        content: '▶';
        font-size: 120%;
        vertical-align: middle;
    }
`;

export const BackButton = Button.extend`
    background-color: ${lighten(0.4, desaturate(0.4, $colorPrimary3))};
    border-bottom-color:  ${lighten(0.4, desaturate(0.4, $colorPrimary4))};
    color: ${$colorForgrand};
    &:hover {
        background-color: ${lighten(0.2, desaturate(0.2, $colorPrimary3))};
        border-bottom-color:  ${lighten(0.2, desaturate(0.2, $colorPrimary4))};
        color: ${$colorForgrand};
    }
    &:before {
        content: '◀';
        font-size: 120%;
        vertical-align: middle;
    }
`;

export const LinkButton = styled.span`
    background-color: ${$colorPrimary3};
    color: ${$colorForgrandInvert};
    padding: 0.25em;
    cursor: pointer;
    border-radius: 3px;
    transition-duration: .3s;
    &:hover {
        transition-duration: .3s;
        background-color: ${$colorPrimary1};
        color: ${$colorPrimary3};
    }
`;

/** input */
export const InputText = styled.input`
    border-bottom: 1px solid ${$colorPrimary2};
    border-left: none;
    border-right: none;
    border-top: none;
    display: inline-block;
    line-height: 1.5em;
    min-height: 1.5em;
    min-width: 10em;
    transition: all 500ms 0ms ease;
    &:focus {
        outline: none !important;
        border-color: ${$colorPrimary3};
    }
`;

export const InputSpan = InputText.withComponent('span');

export const Label = styled.label`
    color: ${lighten(0.3, $colorForgrand)};
    &::after {
        content: ':\\a';
        white-space: pre;
    }
    &.inline {
        &::after {
            content: ':';
        }
    }
`;

export const LabelInline = Label.extend`
    &::after {
        content: ':';
    }
`;

const $tooltip = `
    position: absolute;
    transition: all 0.3s ease 0s;
    opacity: 0;
    z-index: 1000;
`;

export const ToolTip = styled.div`
    position: relative;
    &::before {
        ${$tooltip}
        content: "";
        border: 10px solid transparent;
        border-bottom-color: ${$colorPrimary4};
        top: 10px;
        left: 10px;
    }
    &::after {
        ${$tooltip}
        content: attr(data-tooltip);
        display: block;
        padding: 5px;
        background: ${$colorPrimary4};
        color: ${$colorForgrandInvert};
        border-radius: 5px;
        top: 30px;
        left: 0;
    }
    &:hover {
        &::before {
            top: 16px;
            opacity: 1;
        }
        &::after {
            top: 36px;
            opacity: 1;
        }
    }
`;

// tslint:disable-next-line:no-unused-expression
injectGlobal`
    body,
    html,
    #reactContainer {
        height: 100%;
        font-family: 'Meiryo UI', 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
        font-size: ${$defaultFontSize};
        background-color: ${$colorBackgraound};
        color: ${$colorForgrand};
    }

    p {
        margin: 0;
    }
`;
