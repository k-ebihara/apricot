import Redux from 'redux';

export interface IAsyncAction<T, E> extends Redux.Action {
    onSuccess?: (data: T) => void;
    onError?: (error: any) => void;
}
