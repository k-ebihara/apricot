export const DOC_LIST_API_URL = '/services/v3/apps/{appId}/documentlists/{docListKey}/listview';

export const DOC_LIST_ACCEPTANCE = 'inquiryAcceptanceViewDocList';

export interface IGetDocListDataOptions {
    offset: number;
    limit: number;
    sort?: IDocListSort[];
    search?: SearchCondition[];
}

export interface IDocListSort {
    propertyName: string;
    direction: 'Ascending' | 'Descending' | 'none';
}

export type SearchCondition = ISearchFormula | '(' |')' | 'or' | 'and';

export interface ISearchFormula {
    propertyName: string;
    type: 'string' | 'integer' | 'number' | 'date'
    |   'boolean' | 'id' | 'key' | 'file' | 'image' | 'reader'
    |   'writer' | 'richtext';
    operator: 'equal' | 'notequal' | 'greaterthan' |
        'lessthan' | 'greaterequal' | 'lessequal' | 'range' | 'contain';
    value: string | number | Date | IRangeValue;
}

export interface IRangeValue {
    lower?: string | number | Date;
    upper?: string | number | Date;
    lowerequal?: string | number | Date;
    upperequal?: string | number | Date;
}

export interface IDocListViewResult {
    /** 文書データ */
    docList: IDocListViewResultDocData[];
    /** メトリクス */
    metrics: {
        /** 全件数 */
        totalCount: number,
    };
}

export interface IDocListViewResultDocData {
    /** アプリID */
    appId: string;
    /** 列情報 */
    columnValues: IDocListViewResultColumnValue[];
    /** 文書ID */
    documentId: string;
    /** 文書キー */
    documentKey: string;
    /** 行番号（1始まり） */
    index: number;
    /** カテゴリかどうか */
    isCategory: boolean;
}

export interface IDocListViewResultColumnValue {
    /** プロパティ名 */
    propertyName: string;
    /** 値 */
    value: string;
}

export interface IViewsult {
    docList: Array<{
        /** システム情報 */
        system: {
            /** アプリID */
            appId: string,
            /** 文書ID */
            documentId: string,
            /** 文書キー */
            documentKey: string,
        },
        properties: {
            [key: string]: any,
        },
    }>;
    metrics: {
        totalCount: number;
    };
}
