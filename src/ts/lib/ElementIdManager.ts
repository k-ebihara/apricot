import UUID from 'uuid';

export class ElementIdManager {
    private managedIds: {[key: string]: string} = {};
    public createId = (...key: string[]): string | undefined => {
        let oneid: string | undefined;
        key.forEach((k) => {
            const id = oneid = UUID.v4();
            this.managedIds[k] = id;
        });
        if (key.length === 1) {
            return oneid;
        }
        return undefined;
    }
    public getId = (key: string) => {
        return this.managedIds[key];
    }
}
