export const DOC_API_URL = '/resources/v3/apps/{appId}/documents/{documentId}';
export const ATTACHMENT_FILE_API = '/services/v2/apps/common/files/download/{fileId}';

export interface IGetDocOptions {
    mode: 'creating' | 'updating' | 'browsing';
    lock: 'true' | 'false' | 'force';
    trash: 'true' | 'false';
    form: 'true' | 'false';
}

export interface IGetDocResult<T> {
    system: ISystem | null;
    document: {
        properties: T | null;
    };
}
export interface IRichTextProperty {
    html: string;
    text: string;
    files: string[];
}
/**
 * EIM文書のシステムプロパティ
 *
 * すべてのプロパティを再現していない
 */
export interface ISystem {
    appId: string;
    modelId: string;
    title: string;
    siteId: string;
    formKey: string;
    documentId: string;
    documentKey: string;
    createDatetime: Date;
    lastModifiedDatetime: Date;
    createUserId: string;
    lastModifiedUserId: string;
    attachmentFiles: IAttachemntFile[];
    acl: {
        readers: string[];
        writers: string[];
    };
    createUser: IEimUser;
    lastModifiedUser: IEimUser;
}

export interface IAttachemntFile {
    fileId: string;
    fileName: string;
    fileExtension: string;
    thumbnail: string;
    webView: string;
    fileSize: number;
    lastModified: Date;
    addCompleted: boolean;
}

export interface IEimUser {
    system: ISystem;
    properties: {
        loginUserName: string;
        lastName: string;
        firstName: string;
        phoneticLastName: string;
        phoneticFirstName: string;
        mailAddress: string;
        language: string;
        timezone: string;
        displayName: string;
        dateFormat: string;
        dateSeparator: string;
        timeFormat: string;
        userType: string;
        departmentIds: string[];
        organizationIds: string[];
        groupIds: string[];
        middleName: string;
        faceImage: string;
        mailAddresses: string;
        mailAddresses2: string;
        phone: string;
        phones: string[];
        extension: string;
        extensions: string[];
        faxNumber: string;
        zip: string;
        country: string;
        state: string;
        city: string;
        street: string;
        building: string;
        companyName: string;
        organizationName: string;
        officeKey: string;
        officeName: string;
        ancestorIds: string[];
    };
}

export interface IGetAttachmentFileInfoResult {
    id: string;
    type: 'GET' | 'POST' | 'PUT';
    url: string;
}
