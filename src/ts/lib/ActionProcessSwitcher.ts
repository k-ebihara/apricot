import Clone from 'clone';
import { Action } from 'redux';

type Process<T, A extends Action> = (state: T, action: A) => T;

export class ActionProcessSwitcher<T> {
    /** アクション名をキーとしてそれに対応する処理を保持する */
    private actions: { [actionType: string]: Process<T, any> | undefined } = {};

    public addAction =
        <A extends Action>(actionType: string, process: Process<T, A>): void => {
            this.actions[actionType] = process;
        }

    public processing = (state: T, action: Action): T => {
        const actionProcess = this.actions[action.type];
        if (!!actionProcess) {
            const newState = Clone(state);
            return actionProcess(newState, action);
        } else {
            return state;
        }
    }
}

export default ActionProcessSwitcher;
