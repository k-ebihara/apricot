import fs from 'fs-extra';
import packager from 'electron-packager';

// const ignoreFiles = [
//     '/.git',
//     '/.vscode',
//     '/src',
//     '/node_modules/@types',
//     '/node_modules/.bin',
//     '/.eslintrc.js',
//     '/.gitignore',
//     '/gulpfile.js',
//     '/package-lock.json',
//     '/sequence.md',
//     '/tsconfig.json',
//     '/tslint.json',
// ];

fs.emptyDir('./.package/')
    .then(() => {
        const options = {
            dir: './dist',
            out: './.package',
            // ignore: (filePath) => {
            //     const exist = ignoreFiles.find(ignoreFile => {
            //         return filePath.startsWith(ignoreFile);
            //     });
            //     return !!exist;
            // },
            prune: true,
        };
        return packager(options);
    })
    .then(() => {
        console.log('packaging is complete.');
    })
    .catch(e => {
        console.error('packaging failed.');
        console.error(e);
    });
